<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Magari extends Model
{
    use HasFactory;

    protected $table = 'magari';

    protected $fillable = [
        'title',
        'ad_make_id',
        'subsidiary_id',
        'year_of_manufacture',
        'color',
        'mileage',
        'condition',
        'sec_condition',
        'registered',
        'description',
        'prize',
        'negotiable',
        'images',
        'shown',
        'package',
        'county_id',
        'sub_county_id',
        'transmission',
        'phone_number',
        'slug',
        'name',
    ];

    /**
     * Get all of the admakes for the Magari
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function admakes()
    {
        return $this->hasMany(Subsidiary::class, 'ad_make_id');
    }
}
