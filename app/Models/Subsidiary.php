<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subsidiary extends Model
{
    use HasFactory;

    protected $table = 'subsidiaries';

    protected $fillable = [
        'ad_make_id',
        'model',
        'model_ref',
    ];

    /**
     * Get the admake that owns the Subsidiary
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function admake()
    {
        return $this->belongsTo(AdMake::class, 'ad_make_id', 'id');
    }
}
