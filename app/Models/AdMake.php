<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdMake extends Model
{
    use HasFactory;

    protected $table = 'ad_make';

    protected $fillable = [
        'make_ref',
        'make',
    ];

    /**
     * Get all of the subsidiaries for the AdMake
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subsidiaries()
    {
        return $this->hasMany(Subsidiary::class);
    }
}
