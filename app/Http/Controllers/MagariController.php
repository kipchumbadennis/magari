<?php

namespace App\Http\Controllers;

use App\Models\AdMake;
use App\Models\Counties;
use App\Models\Magari;
use App\Models\SubCounties;
use App\Models\Subsidiary;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
//use Image;

class MagariController extends Controller
{
    public function index()
    {
        $ads = DB::table('magari')
            ->join('subsidiaries', 'subsidiaries.id', '=', 'magari.subsidiary_id')
            ->join('ad_make', 'ad_make.id', '=', 'magari.ad_make_id')
            ->select('magari.shown', 'magari.slug', 'magari.title', 'magari.prize', 'magari.package', 'subsidiaries.model_ref', 'ad_make.make_ref')
            ->get();
        return view('welcome', [
            'ads' => $ads,
        ]);
    }

    public function create()
    {
        $counties = Counties::all();
        $data = AdMake::get();
        return view('pages.create', [
            'data' => $data,
            'counties' => $counties,
        ]);
    }

    public function store(Request $request)
    {
        // $request->validate([
        //     'image' => 'required',
        //     'image*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        //     'category' => 'required',
        //     'model' => 'required',
        //     'county' => 'required',
        //     'subcounty' => 'required',
        //     'title' => 'required',
        //     'year_of_manufacture' => 'required',
        //     'color' => 'required',
        //     'mileage' => 'required',
        //     'transmission' => 'required',
        //     'condition' => 'required',
        //     'conditionDesc' => 'required',
        //     'registered' => 'required',
        //     'prize' => 'required',
        //     'negotiable' => 'required',
        //     'phone_number' => 'required',
        //     'name' => 'required',
        //     'package' => 'required',
        // ]);
        if ($request->hasFile('image')) {
            foreach ($request->file('image') as $image) {
                $originalImage = $image;
//                $image = Image::make($originalImage)->resize(450,225);
//                $image->response('png');
                $image->store('vehicles', ['disk' => 'my_files']);
//                 Image::make($image->store('vehicles', ['disk' => 'my_files']))->resize(450,225);
                $images[] = $originalImage;
            }
        }

        if ($request->negotiable == null || $request->negotiable == '1') {
            $negotiable = 2;
        } else {
            $negotiable = 1;
        }

        return [
            'ad_make_id' => $request->category,
            'subsidiary_id' => $request->model,
            'county_id' => $request->county,
            'sub_county_id' => $request->subcounty,
            'title' => $request->title,
            'year_of_manufacture' => $request->year_of_manufacture,
            'color' => $request->color,
            'mileage' => $request->mileage,
            'transmission' => $request->transmission,
            'condition' => $request->condition,
            'sec_condition' => $request->condition_description,
            'registered' => $request->registered,
            'prize' => $request->prize,
            'negotiable' => $negotiable,
            'phone_number' => $request->phone_number,
            'description' => $request->description,
            'name' => $request->name,
            'package' => $request->package,
            'images' => json_encode($images),
            'shown' => $originalImage,
            'slug' => Str::slug($request->title),
        ];

        Magari::create($data);
        return redirect()->route('home');
    }

    public function cloudinaryResponse(Request $request)
    {
        return $request;
    }

    public function showAd($make, $model, $slug)
    {
        $related = DB::table('magari')
            ->join('subsidiaries', 'subsidiaries.id', '=', 'magari.subsidiary_id')
            ->join('ad_make', 'ad_make.id', '=', 'magari.ad_make_id')
            ->join('counties', 'counties.id', '=', 'magari.county_id')
            ->join('sub_counties', 'sub_counties.id', '=', 'magari.sub_county_id')
            ->select(
                'magari.shown',
                'magari.slug',
                'magari.title',
                'magari.prize',
                'magari.package',
                'subsidiaries.model_ref',
                'ad_make.make_ref',
                'counties.county_name',
                'sub_counties.sub_county_name',
                'magari.description',
            )
            ->where('magari.slug', '!=', $slug)
            ->get();
        $product = Magari::where('slug', $slug)
            ->join('ad_make', 'ad_make.id', '=', 'magari.ad_make_id')
            ->join('subsidiaries', 'subsidiaries.id', '=', 'magari.subsidiary_id')
            ->join('counties', 'counties.id', '=', 'magari.county_id')
            ->join('sub_counties', 'sub_counties.id', '=', 'magari.sub_county_id')
            ->first();
        return view('pages.product', [
            'product' => $product,
            'related_ads' => $related,
        ]);
    }

    public function showMakes($slug)
    {
        return $slug;
    }

    public function show($id)
    {
        return Subsidiary::where('ad_make_id', $id)->get();
    }

    public function searchAds(Request $request)
    {
//        $words = preg_replace('/\W+/', ' ', $search_term);
//        $words = trim($words);
//        $words = explode(' ', $words);

//        DB::table('magari')->whereIn('value', $words)->get();
        $sortlocation = DB::table('counties')->get();
        $makes = AdMake::all();
        return view('pages.searches', [
            'searched' => $request->searchable,
            'sortlocation' => $sortlocation,
            'makes' => $makes,
        ]);
    }

    public function showSearches(Request $request){
        $data = DB::table('magari')
            ->join('subsidiaries', 'subsidiaries.id', '=', 'magari.subsidiary_id')
            ->join('ad_make', 'ad_make.id', '=', 'magari.ad_make_id')
            ->select('magari.shown', 'magari.description', 'magari.slug', 'magari.title', 'magari.prize', 'magari.package', 'subsidiaries.model_ref', 'ad_make.make_ref')
            ->get();
        $resp = $request;
        return ['searched' => $data, 'resp' => $resp];
    }

    public function subCounties($county_id)
    {
        return SubCounties::where('county_id', $county_id)->get();
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
