@extends('layouts.app')

@section('content')
    <div class="bg-lime-600 relative" id="afterNav">
        <!-- mobile menu bar -->
        <div class="md:hidden absolute p-1">
            <!-- mobile menu button -->
            <button class="mobile-menu-button p-1 focus:outline-none focus:bg-gray-700 rounded">
                <svg class="h-5 w-5" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                    stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
                </svg>
            </button>
        </div>
        <div class="py-4 px-8">
            <div class=" py-9">
                <div class="flex justify-center space-x-2 align-middle">
                    <div>Find a vehicle to buy</div>
                    <div>
                        <ul class="space-y-4 text-white">
                            <li class="flex items-center bg-black rounded px-1">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20"
                                    fill="currentColor">
                                    <path fill-rule="evenodd"
                                        d="M5.05 4.05a7 7 0 119.9 9.9L10 18.9l-4.95-4.95a7 7 0 010-9.9zM10 11a2 2 0 100-4 2 2 0 000 4z"
                                        clip-rule="evenodd" />
                                </svg>
                                <p class="ml-1">
                                    All Kenya
                                </p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class=" px-11">
                <form class="w-full bg-white rounded-md" action="{{route('search')}}" method="POST">
                    @csrf
                    <div class="flex items-center border-b border-teal-500 py-2 px-3">
                        <input
                            class="appearance-none bg-transparent border-none w-full text-gray-700 mr-3 py-1 px-2 leading-tight focus:outline-none"
                            type="text" placeholder="What are you looking for?" aria-label="Car search" name="searchable">
                        <button
                            class="flex-shrink-0 bg-teal-500 hover:bg-teal-700 border-teal-500 hover:border-teal-700 text-sm border-4 text-white py-1 px-2 rounded"
                            type="submit">
                            Search
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="relative md:flex md:space-x-2 p-4">
        @include('pages.aside')
        <!-- content -->
        <div class="md:w-3/4 w-full py-1">
            {{-- {{ $ads }} --}}
            <div class="grid lg:grid-cols-4 md:grid-cols-3 sm:grid-cols-2 gap-4">
                @isset($ads)
                    @foreach ($ads as $ad)
                        <a href="{{ route('ad.show', ['make' => $ad->make_ref, 'model' => $ad->model_ref, 'slug' => $ad->slug]) }}"
                            class="bg-white rounded bg-cover h-60 md:h-56 lg:h-56 relative"
                            style="background-image: url('{{ asset('vehicles/') . '/' . $ad->shown }}')">
                            <div class="absolute bottom-0 space-y-0 px-2 bg-white w-full rounded-b">
                                <div class="text-black -mb-1">{{ $ad->title }}</div>
                                <div class=" text-red-600">{{ __('Ksh. ') . number_format($ad->prize) }}</div>
                            </div>
                            <div class="absolute right-2 bottom-8 text-red-400">
                                <button class="rounded-full bg-white hover:scale-125 hover:bg-slate-600 hover:text-white">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" viewBox="0 0 20 20"
                                        fill="currentColor">
                                        <path
                                            d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
                                    </svg>
                                </button>
                            </div>
                            <div class="rounded-tr roulded-bl bg-orange-500 text-center w-14 ml-auto items-center">
                                <div class="text-white">
                                    <p p-1>{{$ad->package}}</p>
                                </div>
                            </div>
                        </a>
                    @endforeach
                @endisset
            </div>
        </div>
    </div>
@endsection
