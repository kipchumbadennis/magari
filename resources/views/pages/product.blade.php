@extends('layouts.app')
@section('content')
    <div class="py-3 md:px-10 lg:px-28 px-4">
        <nav class="flex" aria-label="Breadcrumb">
            <!-- mobile menu bar -->
            <div class="flex justify-between md:hidden">
                <!-- mobile menu button -->
                <button class="mobile-menu-button p-1 focus:outline-none focus:bg-gray-700">
                    <svg class="h-5 w-5" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                         stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                              d="M4 6h16M4 12h16M4 18h16"/>
                    </svg>
                </button>
            </div>
            <ol class="inline-flex items-center space-x-1 md:space-x-3 px-2">
                <li class="inline-flex items-center">
                    <a href="{{ route('homepage') }}"
                       class="inline-flex items-center text-sm font-medium text-gray-700 hover:text-gray-900 dark:text-gray-400 dark:hover:text-white">
                        {{-- <svg class="mr-2 w-4 h-4" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M10.707 2.293a1 1 0 00-1.414 0l-7 7a1 1 0 001.414 1.414L4 10.414V17a1 1 0 001 1h2a1 1 0 001-1v-2a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 001 1h2a1 1 0 001-1v-6.586l.293.293a1 1 0 001.414-1.414l-7-7z"></path></svg> --}}
                        All ads
                    </a>
                </li>
                <li>
                    <div class="flex items-center">
                        <svg class="w-6 h-6 text-gray-400" fill="currentColor" viewBox="0 0 20 20"
                             xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd"
                                  d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                  clip-rule="evenodd"></path>
                        </svg>
                        <a href="{{ route('make.show', ['slug' => 'toyota']) }}"
                           class="ml-1 text-sm font-medium text-gray-700 hover:text-gray-900 md:ml-2 dark:text-gray-400 dark:hover:text-white">{{ $product->make }}</a>
                    </div>
                </li>
                <li aria-current="page">
                    <div class="flex items-center">
                        <svg class="w-6 h-6 text-gray-400" fill="currentColor" viewBox="0 0 20 20"
                             xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd"
                                  d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                  clip-rule="evenodd"></path>
                        </svg>
                        <span
                            class="ml-1 text-sm font-medium text-gray-400 md:ml-2 dark:text-gray-500">{{ $product->model }}</span>
                    </div>
                </li>
            </ol>
        </nav>
    </div>
    <main class="py-3 md:px-10 lg:px-28 px-4 relative">
        {{-- {{ $product }} --}}
        <div class=" md:hidden bg-gray-400">
            @include('pages.aside')
        </div>
        <div class="grid md:grid-cols-3 gap-4">
            <div class="md:col-span-2">
                <div class="bg-white">
                    <div class="border-t-4 border-orange-500">
                        <img src="{{ asset('vehicles/' . $product->shown) }}" alt="" id="displayPosition"
                             class="w-full">
                    </div>
                    <div class="py-4 px-5">
                        <div id="otherImages" class="flex space-x-2 overflow-x-auto">
                            @foreach (json_decode($product->images) as $item)
                                <img src="{{ asset('vehicles/' . $item) }}" alt=""
                                     class="displayImage h-14 rounded cursor-pointer" id="displayImage">
                            @endforeach
                        </div>
                        <div class="space-y-4 divide-y divider-red-500">
                            <div>
                                <div class="flex justify-between items-center py-2">
                                    <p class="capitalize">{{ $product->title }}</p>
                                    <div class="flex justify-center items-center space-x-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none"
                                             viewBox="0 0 24 24" stroke="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                                  d="M11.049 2.927c.3-.921 1.603-.921 1.902 0l1.519 4.674a1 1 0 00.95.69h4.915c.969 0 1.371 1.24.588 1.81l-3.976 2.888a1 1 0 00-.363 1.118l1.518 4.674c.3.922-.755 1.688-1.538 1.118l-3.976-2.888a1 1 0 00-1.176 0l-3.976 2.888c-.783.57-1.838-.197-1.538-1.118l1.518-4.674a1 1 0 00-.363-1.118l-3.976-2.888c-.784-.57-.38-1.81.588-1.81h4.914a1 1 0 00.951-.69l1.519-4.674z"/>
                                        </svg>
                                        <p>230</p>
                                    </div>
                                </div>
                                <div class="flex justify-between items-center text-xs py-2">
                                    <div class="flex justify-start space-x-3">
                                        <p class="border border-gray-400 rounded px-2">{{ $product->package }}</p>
                                        <p class=" inline-flex space-x-2">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none"
                                                 viewBox="0 0 24 24" stroke="currentColor">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                                      d="M12 8v4l3 3m6-3a9 9 0 11-18 0 9 9 0 0118 0z"/>
                                            </svg>
                                            <span>{{ $product->created_at->diffForHumans() }}</span>
                                        </p>
                                        <p class="inline-flex space-x-2">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none"
                                                 viewBox="0 0 24 24" stroke="currentColor">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                                      d="M17.657 16.657L13.414 20.9a1.998 1.998 0 01-2.827 0l-4.244-4.243a8 8 0 1111.314 0z"/>
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                                      d="M15 11a3 3 0 11-6 0 3 3 0 016 0z"/>
                                            </svg>
                                            <span>{{ $product->county_name . ', ' . $product->sub_county_name }}</span>
                                        </p>
                                    </div>
                                    <div class="flex justify-center items-center space-x-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" viewBox="0 0 20 20"
                                             fill="currentColor">
                                            <path d="M10 12a2 2 0 100-4 2 2 0 000 4z"/>
                                            <path fill-rule="evenodd"
                                                  d="M.458 10C1.732 5.943 5.522 3 10 3s8.268 2.943 9.542 7c-1.274 4.057-5.064 7-9.542 7S1.732 14.057.458 10zM14 10a4 4 0 11-8 0 4 4 0 018 0z"
                                                  clip-rule="evenodd"/>
                                        </svg>
                                        <p>1,000</p>
                                    </div>
                                </div>
                            </div>
                            <div class="flex items-center justify-start space-x-4 py-2">
                                <div class="items-center flex flex-col space-y-2">
                                    <svg width="50" height="50" viewBox="0 0 50 50" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M25 49.5C38.531 49.5 49.5 38.531 49.5 25C49.5 11.469 38.531 0.5 25 0.5C11.469 0.5 0.5 11.469 0.5 25C0.5 38.531 11.469 49.5 25 49.5Z"
                                            fill="white" stroke="#EEF2F4"></path>
                                        <path d="M30 18H21.5L22.3478 19.6667L23.7391 20L29.3043 19.6667L30 18Z"
                                              fill="#FFA010">
                                        </path>
                                        <path
                                            d="M38.6204 25.9899C38.3803 25.6909 37.4488 24.8044 35.5283 25.9052C35.5152 25.9132 35.5048 25.9211 35.4917 25.929L31.3167 28.9114C31.0532 28.4986 30.5261 28.2392 29.8868 28.2392C28.5743 28.2392 26.7608 27.8238 26.4555 27.7523C24.8246 26.9505 23.0503 26.8552 21.3228 27.4692C20.3 27.8343 19.5772 28.3451 19.2928 28.5674L17.307 28.5303V27.2231C17.307 26.9823 17.1139 26.7891 16.8791 26.7891H12.4484C12.3101 26.7891 12.1822 26.8552 12.1013 26.969C10.5618 29.1151 10.0712 31.6741 10.687 34.3732C11.1463 36.395 12.0543 37.771 12.0935 37.8266C12.1744 37.9457 12.3048 38.0171 12.4484 38.0171H16.8765C17.1139 38.0171 17.3044 37.8213 17.3044 37.5831V36.4003C18.5021 36.6067 21.5968 37.0777 24.7933 37.0777C26.4424 37.0777 28.115 36.9533 29.5685 36.5987C30.2104 36.4426 30.9462 35.9663 31.8204 35.1406C34.3958 32.7114 36.7938 30.2319 38.5708 28.1598C39.0979 27.5459 39.1214 26.6118 38.6204 25.9899ZM12.6858 37.1491C12.4379 36.7257 11.8534 35.6276 11.522 34.1774C10.9741 31.772 11.3603 29.5782 12.6702 27.657H16.4485V37.1491H12.6858ZM37.9237 27.5909C36.165 29.6391 33.7878 32.0975 31.2359 34.5055C30.2991 35.3894 29.7015 35.6725 29.3675 35.7546C25.1899 36.7681 18.8492 35.7837 17.3044 35.5191V29.3983L19.431 29.438C19.538 29.4406 19.6398 29.4036 19.7181 29.3321C19.7468 29.3057 22.6562 26.8208 26.1215 28.5541C26.1502 28.5674 26.1789 28.5779 26.2102 28.5859C26.2963 28.6071 28.3551 29.1072 29.8868 29.1072C30.3278 29.1072 30.6044 29.3004 30.6383 29.483C30.6931 29.774 30.2652 30.2848 29.1953 30.7188C27.6245 31.3565 25.6831 30.7161 25.6648 30.7108C25.4404 30.6341 25.1978 30.7585 25.1221 30.986C25.0464 31.2136 25.1691 31.4597 25.3935 31.5365C25.4535 31.5576 26.4085 31.8752 27.5853 31.8725C28.1985 31.8725 28.8744 31.7852 29.5137 31.5259C30.8653 30.9781 31.322 30.343 31.4524 29.8667C31.4785 29.8561 31.502 29.8402 31.5255 29.8243L35.9667 26.6541C36.8486 26.154 37.608 26.109 37.955 26.5403C38.1951 26.8394 38.182 27.2892 37.9237 27.5909Z"
                                            fill="#303A4B"></path>
                                        <path
                                            d="M14.4341 33.6331C13.6225 33.6331 12.965 34.3026 12.965 35.1229C12.965 35.9459 13.6252 36.6128 14.4341 36.6128C15.2456 36.6128 15.9031 35.9433 15.9031 35.1229C15.9057 34.3026 15.2456 33.6331 14.4341 33.6331ZM14.4341 35.7448C14.0975 35.7448 13.8209 35.4669 13.8209 35.1229C13.8209 34.7816 14.0948 34.5011 14.4341 34.5011C14.7707 34.5011 15.0473 34.7789 15.0473 35.1229C15.0473 35.4669 14.7733 35.7448 14.4341 35.7448Z"
                                            fill="#303A4B"></path>
                                        <path
                                            d="M19.1052 22.0534C19.567 22.0534 19.9428 21.6723 19.9428 21.2039C19.9428 20.7356 19.567 20.3545 19.1052 20.3545C18.6433 20.3545 18.2676 20.7356 18.2676 21.2039C18.2676 21.6723 18.6433 22.0534 19.1052 22.0534ZM19.1052 21.1854C19.1156 21.1854 19.1235 21.1934 19.1235 21.2039C19.1235 21.2251 19.0869 21.2251 19.0869 21.2039C19.0869 21.1934 19.0948 21.1854 19.1052 21.1854Z"
                                            fill="#303A4B"></path>
                                        <path
                                            d="M17.1738 24.5059C17.1738 24.9637 17.5391 25.3341 17.9906 25.3341H19.5353C19.9867 25.3341 20.352 24.9637 20.352 24.5059V23.577H30.9852V24.5059C30.9852 24.9637 31.3506 25.3341 31.802 25.3341H33.3467C33.7981 25.3341 34.1635 24.9637 34.1635 24.5059V18.6629C34.1635 17.742 33.7616 16.8873 33.0727 16.3131L33.0806 16.2945L33.8582 16.059C34.0408 16.0035 34.1635 15.8341 34.1635 15.6436V15.027C34.1635 14.7862 33.9704 14.593 33.7355 14.593H32.2325C32.0734 14.593 31.9324 14.683 31.8594 14.8126L30.9148 13.1958C30.5338 12.5448 29.8371 12.1426 29.0908 12.1426H22.2438C21.5002 12.1426 20.8009 12.5448 20.4225 13.1958L19.4779 14.8126C19.4048 14.6803 19.2639 14.593 19.1048 14.593H17.6018C17.3643 14.593 17.1738 14.7888 17.1738 15.027V15.6436C17.1738 15.8367 17.2991 16.0061 17.4791 16.059L18.2567 16.2945L18.2645 16.3131C17.5757 16.8873 17.1738 17.742 17.1738 18.6629V24.5059ZM20.0311 18.8826L20.112 19.0784L18.0297 18.6603C18.0297 18.3322 18.1028 18.0173 18.2332 17.7341L20.0311 18.8826ZM20.7435 20.0893C20.7722 20.0946 20.7982 20.0972 20.827 20.0972C20.9548 20.0972 21.0775 20.039 21.161 19.9358C21.2601 19.8114 21.2836 19.6394 21.221 19.4912L20.7669 18.4142C20.733 18.3322 20.6756 18.2634 20.6025 18.2184L18.7525 17.0355C18.836 16.9614 18.9221 16.8926 19.016 16.8317L19.2039 16.7391H32.1203C32.1229 16.7391 32.1255 16.7391 32.1308 16.7391L32.3212 16.8317C32.4152 16.8926 32.5039 16.9614 32.5848 17.0355L30.7347 18.221C30.6617 18.2687 30.6043 18.3375 30.5704 18.4168L30.1163 19.4939C30.0537 19.6421 30.0772 19.8141 30.1763 19.9384C30.2598 20.0416 30.3825 20.0999 30.5103 20.0999C30.539 20.0999 30.5651 20.0972 30.5938 20.0919L33.3076 19.5468V22.0184L32.3891 22.7091H18.9508L18.0323 22.0184V19.5468L20.7435 20.0893ZM33.305 18.6576L31.2227 19.0758L31.3036 18.8799L33.1014 17.7288C33.2319 18.0146 33.305 18.3295 33.305 18.6576ZM18.0297 24.4662V23.0981L18.5516 23.4897C18.6246 23.5453 18.716 23.5744 18.8073 23.5744H19.4962V24.4662H18.0297ZM31.8411 23.577H32.53C32.6213 23.577 32.7127 23.5479 32.7857 23.4924L33.3076 23.1007V24.4688H31.8411V23.577ZM32.6579 15.461H32.8353L32.6579 15.5139V15.461ZM21.1583 13.6377C21.3854 13.2514 21.8002 13.0105 22.2438 13.0105H29.0908C29.5344 13.0105 29.9493 13.2514 30.1763 13.6377L31.4784 15.8685H19.8563L21.1583 13.6377ZM18.6768 15.461V15.5139L18.4994 15.461H18.6768Z"
                                            fill="#303A4B"></path>
                                        <path
                                            d="M22.2461 20.3277L25.5705 20.6479C25.5992 20.6532 25.6279 20.6559 25.6566 20.6559H25.6827C25.7114 20.6559 25.7401 20.6532 25.7688 20.6479L29.0931 20.3277C29.2419 20.3145 29.3723 20.2219 29.4376 20.0869L30.4291 18.052C30.4944 17.917 30.4865 17.7582 30.4082 17.6312C30.33 17.5042 30.1917 17.4248 30.0455 17.4248H21.2937C21.145 17.4248 21.0067 17.5015 20.931 17.6312C20.8527 17.7582 20.8449 17.9197 20.9101 18.052L21.9017 20.0869C21.9669 20.2245 22.0974 20.3145 22.2461 20.3277ZM29.3514 18.2954L28.7696 19.4862L25.667 19.7826L22.5645 19.4836L21.9826 18.2928H29.3514V18.2954Z"
                                            fill="#303A4B"></path>
                                        <path
                                            d="M32.2292 22.0534C32.6911 22.0534 33.0668 21.6723 33.0668 21.2039C33.0668 20.7356 32.6911 20.3545 32.2292 20.3545C31.7673 20.3545 31.3916 20.7356 31.3916 21.2039C31.3916 21.6723 31.7673 22.0534 32.2292 22.0534ZM32.2292 21.1854C32.2396 21.1854 32.2475 21.1934 32.2475 21.2039C32.2475 21.2251 32.2109 21.2251 32.2109 21.2039C32.2109 21.1934 32.2188 21.1854 32.2292 21.1854Z"
                                            fill="#303A4B"></path>
                                        <path
                                            d="M27.6313 21.1846H23.592C23.3545 21.1846 23.1641 21.3804 23.1641 21.6186C23.1641 21.8594 23.3572 22.0525 23.592 22.0525H27.6313C27.8688 22.0525 28.0593 21.8567 28.0593 21.6186C28.0593 21.3804 27.8688 21.1846 27.6313 21.1846Z"
                                            fill="#303A4B"></path>
                                    </svg>
                                    <p class=" text-xs text-gray-400">
                                        @if ($product->condition == 'kenya')
                                            Kenyan Used
                                        @elseif ($product->condition == 'foreign')
                                            Foreign Used
                                        @else
                                            Brand New
                                        @endif
                                    </p>
                                </div>
                                <div class="items-center flex flex-col space-y-2">
                                    <svg width="50" height="50" viewBox="0 0 50 50" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M25 49.5C38.531 49.5 49.5 38.531 49.5 25C49.5 11.469 38.531 0.5 25 0.5C11.469 0.5 0.5 11.469 0.5 25C0.5 38.531 11.469 49.5 25 49.5Z"
                                            fill="white" stroke="#EEF2F4"></path>
                                        <path
                                            d="M23.8078 13H21.4232L19.8335 14L19.436 15L20.2309 15.5L21.4232 15L23.013 14.5L24.2053 13.5L23.8078 13Z"
                                            fill="#FFA010"></path>
                                        <path
                                            d="M33.1636 33.4644H32.06V32.543C32.06 31.9118 31.5317 31.3977 30.8831 31.3977H29.6513V30.2905C29.6513 29.8069 29.2486 29.415 28.7517 29.415H26.398L24.8132 22.0516C25.263 20.5041 25.6788 16.4318 25.3257 14.7316C25.025 13.2681 23.7618 11.7384 21.2094 12.2398C19.2139 12.6343 17.7965 13.2044 17.4225 14.6221C17.4173 14.6374 17.4147 14.6527 17.412 14.6679C17.3833 14.7901 17.3597 14.9174 17.344 15.0548C17.2159 16.2511 17.9979 18.0353 19.5225 18.9566C20.8275 19.7431 21.7141 22.3291 21.7926 22.5683L23.2179 29.415H21.1937C20.6968 29.415 20.294 29.8069 20.294 30.2905V31.3977H19.0623C18.4137 31.3977 17.8854 31.9118 17.8854 32.543V33.4644H16.8367C16.6118 33.4644 16.4287 33.6426 16.4287 33.8615V36.6027C16.4287 36.8216 16.6118 36.9997 16.8367 36.9997H33.1636C33.3885 36.9997 33.5716 36.8216 33.5716 36.6027V33.8615C33.5716 33.6426 33.3885 33.4644 33.1636 33.4644ZM22.6713 22.777L24.0783 22.4996L24.1907 23.0214L22.7811 23.2988L22.6713 22.777ZM21.3715 13.0212C21.6618 12.9652 21.9233 12.9397 22.1639 12.9397C22.9093 12.9397 23.4245 13.1892 23.7801 13.5149C23.7723 13.6778 23.5945 13.8509 23.4375 13.9731C22.2424 14.8996 19.159 15.3933 18.3404 14.953C18.2201 14.8894 18.2149 14.841 18.2175 14.7977C18.4267 14.0189 19.1302 13.4615 21.3715 13.0212ZM19.954 18.2796C18.9315 17.6637 18.3561 16.5819 18.1966 15.7522C18.5157 15.8616 18.9184 15.9125 19.3682 15.9125C20.9217 15.9125 23.0086 15.3221 23.9475 14.5941C24.0992 14.4745 24.2195 14.3574 24.311 14.2429C24.4339 14.5 24.4967 14.7367 24.5281 14.8868C24.8445 16.4114 24.4732 20.1529 24.0678 21.6902L22.4594 22.0084C22.1535 21.1761 21.2695 19.0737 19.954 18.2796ZM22.9406 24.0802L24.3555 23.8002L25.5637 29.4124H24.0495L22.9406 24.0802ZM21.1126 30.288C21.1126 30.2447 21.1518 30.2065 21.1963 30.2065H28.7543C28.7988 30.2065 28.838 30.2447 28.838 30.288V31.3951H21.1126V30.288ZM18.704 32.5405C18.704 32.3471 18.8661 32.1892 19.0649 32.1892H30.8857C31.0845 32.1892 31.2466 32.3471 31.2466 32.5405V33.4619H18.704V32.5405ZM32.7556 36.2056H17.2447V34.2585H32.7556V36.2056V36.2056Z"
                                            fill="#303A4B"></path>
                                    </svg>
                                    <p class=" text-xs text-gray-400">
                                        @if ($product->transmission == 'manual')
                                            {{ __('Manual') }}
                                        @else
                                            {{ __('Automatic') }}
                                        @endif
                                    </p>
                                </div>
                            </div>
                            <div class="flex justify-start space-x-6 py-4">
                                <div class="flex flex-col space-y-2">
                                    <div class=" space-y-1">
                                        <p class="uppercase text-gray-400 text-xs">Parts condition</p>
                                        <p class="text-sm">
                                            @if ($product->condition == 'new')
                                                New
                                            @else
                                                Used
                                            @endif
                                        </p>
                                    </div>
                                    <div class=" space-y-1">
                                        <p class="uppercase text-gray-400 text-xs">Model</p>
                                        <p class="text-sm">{{ $product->model }}</p>
                                    </div>
                                    <div class=" space-y-1">
                                        <p class="uppercase text-gray-400 text-xs">color</p>
                                        <p class="text-sm">{{ $product->color }}</p>
                                    </div>
                                </div>
                                <div class="flex flex-col space-y-2">
                                    <div class=" space-y-1">
                                        <p class="uppercase text-gray-400 text-xs">Make</p>
                                        <p class="text-sm">{{ $product->make }}</p>
                                    </div>
                                    <div class=" space-y-1">
                                        <p class="uppercase text-gray-400 text-xs">year of manufacture</p>
                                        <p class="text-sm">{{ $product->year_of_manufacture }}</p>
                                    </div>
                                    <div class=" space-y-1">
                                        <p class="uppercase text-gray-400 text-xs">registered vehicle</p>
                                        <p class="text-sm">
                                            @if ($product->registered == '2')
                                                Yes
                                            @else
                                                No
                                            @endif
                                        </p>
                                    </div>
                                </div>
                                <div class="flex flex-col space-y-2">
                                    <div class=" space-y-1">
                                        <p class="uppercase text-gray-400 text-xs">distance</p>
                                        <p class="text-sm">{{ number_format($product->mileage) . __(' Km') }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="flex flex-col space-y-3 py-4">
                                <div class=" text-gray-800">
                                    <p>
                                        {{ $product->description }}
                                    </p>
                                </div>
                                <a href="tel:0715738974"
                                   class="bg-orange-400 hidden rounded text-white hover:bg-orange-700 items-center py-2 px-4 w-40 space-x-2 justify-center"
                                   id="hiddenBtnDown">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none"
                                         viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                              d="M16 3h5m0 0v5m0-5l-6 6M5 3a2 2 0 00-2 2v1c0 8.284 6.716 15 15 15h1a2 2 0 002-2v-3.28a1 1 0 00-.684-.948l-4.493-1.498a1 1 0 00-1.21.502l-1.13 2.257a11.042 11.042 0 01-5.516-5.517l2.257-1.128a1 1 0 00.502-1.21L9.228 3.683A1 1 0 008.279 3H5z"/>
                                    </svg>
                                    <span>0715738974</span>
                                </a>
                                <button
                                    class="bg-orange-400 rounded text-white hover:bg-orange-700 items-center py-2 w-40 space-x-2 flex justify-center"
                                    id="showContactDown">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none"
                                         viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                              d="M16 3h5m0 0v5m0-5l-6 6M5 3a2 2 0 00-2 2v1c0 8.284 6.716 15 15 15h1a2 2 0 002-2v-3.28a1 1 0 00-.684-.948l-4.493-1.498a1 1 0 00-1.21.502l-1.13 2.257a11.042 11.042 0 01-5.516-5.517l2.257-1.128a1 1 0 00.502-1.21L9.228 3.683A1 1 0 008.279 3H5z"/>
                                    </svg>
                                    <span>Show Contact</span>
                                </button>
                            </div>
                            <div class="py-4 flex justify-start space-x-3">
                                <button class="py-1 px-4 bg-orange-400 rounded text-white">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" fill="none"
                                         viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                              d="M3 8l7.89 5.26a2 2 0 002.22 0L21 8M5 19h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z"/>
                                    </svg>
                                </button>
                                <button class="py-1 px-4 bg-blue-700 rounded text-white">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                         class="bi bi-facebook" viewBox="0 0 16 16">
                                        <path
                                            d="M16 8.049c0-4.446-3.582-8.05-8-8.05C3.58 0-.002 3.603-.002 8.05c0 4.017 2.926 7.347 6.75 7.951v-5.625h-2.03V8.05H6.75V6.275c0-2.017 1.195-3.131 3.022-3.131.876 0 1.791.157 1.791.157v1.98h-1.009c-.993 0-1.303.621-1.303 1.258v1.51h2.218l-.354 2.326H9.25V16c3.824-.604 6.75-3.934 6.75-7.951z"/>
                                    </svg>
                                </button>
                                <button class="py-1 px-4 bg-blue-500 rounded text-white">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                         class="bi bi-twitter" viewBox="0 0 16 16">
                                        <path
                                            d="M5.026 15c6.038 0 9.341-5.003 9.341-9.334 0-.14 0-.282-.006-.422A6.685 6.685 0 0 0 16 3.542a6.658 6.658 0 0 1-1.889.518 3.301 3.301 0 0 0 1.447-1.817 6.533 6.533 0 0 1-2.087.793A3.286 3.286 0 0 0 7.875 6.03a9.325 9.325 0 0 1-6.767-3.429 3.289 3.289 0 0 0 1.018 4.382A3.323 3.323 0 0 1 .64 6.575v.045a3.288 3.288 0 0 0 2.632 3.218 3.203 3.203 0 0 1-.865.115 3.23 3.23 0 0 1-.614-.057 3.283 3.283 0 0 0 3.067 2.277A6.588 6.588 0 0 1 .78 13.58a6.32 6.32 0 0 1-.78-.045A9.344 9.344 0 0 0 5.026 15z"/>
                                    </svg>
                                </button>
                                <button class="py-1 px-4 bg-orange-400 rounded text-white">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                         class="bi bi-whatsapp" viewBox="0 0 16 16">
                                        <path
                                            d="M13.601 2.326A7.854 7.854 0 0 0 7.994 0C3.627 0 .068 3.558.064 7.926c0 1.399.366 2.76 1.057 3.965L0 16l4.204-1.102a7.933 7.933 0 0 0 3.79.965h.004c4.368 0 7.926-3.558 7.93-7.93A7.898 7.898 0 0 0 13.6 2.326zM7.994 14.521a6.573 6.573 0 0 1-3.356-.92l-.24-.144-2.494.654.666-2.433-.156-.251a6.56 6.56 0 0 1-1.007-3.505c0-3.626 2.957-6.584 6.591-6.584a6.56 6.56 0 0 1 4.66 1.931 6.557 6.557 0 0 1 1.928 4.66c-.004 3.639-2.961 6.592-6.592 6.592zm3.615-4.934c-.197-.099-1.17-.578-1.353-.646-.182-.065-.315-.099-.445.099-.133.197-.513.646-.627.775-.114.133-.232.148-.43.05-.197-.1-.836-.308-1.592-.985-.59-.525-.985-1.175-1.103-1.372-.114-.198-.011-.304.088-.403.087-.088.197-.232.296-.346.1-.114.133-.198.198-.33.065-.134.034-.248-.015-.347-.05-.099-.445-1.076-.612-1.47-.16-.389-.323-.335-.445-.34-.114-.007-.247-.007-.38-.007a.729.729 0 0 0-.529.247c-.182.198-.691.677-.691 1.654 0 .977.71 1.916.81 2.049.098.133 1.394 2.132 3.383 2.992.47.205.84.326 1.129.418.475.152.904.129 1.246.08.38-.058 1.171-.48 1.338-.943.164-.464.164-.86.114-.943-.049-.084-.182-.133-.38-.232z"/>
                                    </svg>
                                </button>
                            </div>
                            <div class="py-4 w-1/2">
                                <p id="successMessage" class=" text-xs text-gray-400 pb-3"></p>
                                <button class="bg-transparent border border-orange-500 rounded py-1 px-4"
                                        id="callBackRequest">
                                    Request callback
                                </button>
                                <form action="" method="post" id="requestCallForm"
                                      class="hidden relative transition duration-300 ease-in-out">
                                    @csrf
                                    <button class="absolute top-1 right-1 text-orange-400 z-50" id="closeForm">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                             fill="currentColor"
                                             class="bi bi-x" viewBox="0 0 16 16">
                                            <path
                                                d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
                                        </svg>
                                    </button>
                                    <div class="relative z-0 mb-6 w-full group">
                                        <label for="userName"></label><input type="text" name="name" id="userName"
                                                                             class="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                                                                             placeholder=" " required/>
                                        <label for="name"
                                               class="absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">Your
                                            name</label>
                                    </div>
                                    <div class="relative z-0 mb-6 w-full group">
                                        <label for="phoneNumber"></label><input type="text" name="phone_number"
                                                                                id="phoneNumber"
                                                                                class="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                                                                                placeholder=" " required/>
                                        <label for="phone_number"
                                               class="absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">Contact
                                            number</label>
                                    </div>
                                    <div class="mb-6">
                                        <button type="submit" id="submitBtn"
                                                class=" bg-orange-300 rounded px-3 py-1">Submit
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="">
                <div class="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-1 md:space-y-4 sm:space-x-4 md:space-x-0">
                    <div class="space-y-2">
                        <div class="bg-white">
                            <div class="py-4 px-8 space-y-3">
                                <p class="md:text-2xl text-lg font-bold">{{ __('Ksh. ') . number_format($product->prize) }}
                                </p>
                                <button
                                    class="rounded hover:bg-orange-400 px-4 py-2 text-orange-800 border border-orange-400 hover:text-white w-full">
                                    Make an offer
                                </button>
                            </div>
                        </div>
                        <div class="bg-white">
                            <div class="px-8 py-3 space-y-2">
                                <div class="flex flex-col md:flex-row md:justify-start space-x-4 items-center">
                                    <img src="{{ asset('assets/profile/me.jpg') }}" alt=""
                                         class=" h-16 w-16 rounded-full">
                                    <div class=" md:flex md:flex-col space-y-3">
                                        <p class="text-sm">Dennis Kipchumba</p>
                                        <div
                                            class="flex justify-center space-x-1 items-center text-xs bg-gray-400 rounded md:px-0 px-1 py-1">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4"
                                                 viewBox="0 0 20 20" fill="white">
                                                <path fill-rule="evenodd"
                                                      d="M6.267 3.455a3.066 3.066 0 001.745-.723 3.066 3.066 0 013.976 0 3.066 3.066 0 001.745.723 3.066 3.066 0 012.812 2.812c.051.643.304 1.254.723 1.745a3.066 3.066 0 010 3.976 3.066 3.066 0 00-.723 1.745 3.066 3.066 0 01-2.812 2.812 3.066 3.066 0 00-1.745.723 3.066 3.066 0 01-3.976 0 3.066 3.066 0 00-1.745-.723 3.066 3.066 0 01-2.812-2.812 3.066 3.066 0 00-.723-1.745 3.066 3.066 0 010-3.976 3.066 3.066 0 00.723-1.745 3.066 3.066 0 012.812-2.812zm7.44 5.252a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
                                                      clip-rule="evenodd"/>
                                            </svg>
                                            <span>3 months</span>
                                        </div>
                                    </div>
                                </div>
                                <a href="tel:0715738974"
                                   class="bg-orange-400 hidden rounded text-white hover:bg-orange-700 items-center py-2 px-4 w-full space-x-2 justify-center"
                                   id="showContactHref">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none"
                                         viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                              d="M16 3h5m0 0v5m0-5l-6 6M5 3a2 2 0 00-2 2v1c0 8.284 6.716 15 15 15h1a2 2 0 002-2v-3.28a1 1 0 00-.684-.948l-4.493-1.498a1 1 0 00-1.21.502l-1.13 2.257a11.042 11.042 0 01-5.516-5.517l2.257-1.128a1 1 0 00.502-1.21L9.228 3.683A1 1 0 008.279 3H5z"/>
                                    </svg>
                                    <span>0715738974</span>
                                </a>
                                <button
                                    class="bg-orange-400 rounded text-white hover:bg-orange-700 items-center py-2 px-4 w-full space-x-2 flex justify-center"
                                    id="showContact">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none"
                                         viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                              d="M16 3h5m0 0v5m0-5l-6 6M5 3a2 2 0 00-2 2v1c0 8.284 6.716 15 15 15h1a2 2 0 002-2v-3.28a1 1 0 00-.684-.948l-4.493-1.498a1 1 0 00-1.21.502l-1.13 2.257a11.042 11.042 0 01-5.516-5.517l2.257-1.128a1 1 0 00.502-1.21L9.228 3.683A1 1 0 008.279 3H5z"/>
                                    </svg>
                                    <span>Show Contact</span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="space-y-2">
                        <div class="bg-white">
                            <div class="px-8 md:py-3 space-y-2">
                                <div class="font-bold items-center">
                                    <p>Safety Tips</p>
                                </div>
                                <ul class=" list-disc">
                                    <li>Don't send any prepayments</li>
                                    <li>Meet with the seller at a safe public place</li>
                                    <li>Inspect what you're going to buy to make sure it's what you need</li>
                                    <li>Check all the docs and only pay if you're satisfied</li>
                                </ul>
                            </div>
                        </div>
                        <div class="bg-white">
                            <div class="px-8 py-3 space-y-2">
                                <button
                                    class="rounded border border-orange-500 w-full py-2 px-4 hover:bg-orange-700 hover:text-white text-orange-500">
                                    Post
                                    Ad Like This
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <p class="text-lg text-gray-700 py-3">Related ads</p>
        @isset($related_ads)
            @foreach ($related_ads as $related)
                <a href="{{ route('ad.show', ['make' => $related->make_ref, 'model' => $related->model_ref, 'slug' => $related->slug]) }}">
                    <div class="w-full lg:w-4/6 md:flex mb-4">
                        <div
                            class=" h-56 md:h-auto md:w-52 flex-none bg-cover rounded-t md:rounded-t-none md:rounded-l text-center overflow-hidden bg-slate-700"
                            style="background-image: url('{{ asset('vehicles/'.$related->shown) }}')" title="Motor">
                        </div>
                        <div
                            class="border-r border-b border-l border-gray-400 md:border-l-0 md:border-t md:border-gray-400 bg-white rounded-b md:rounded-b-none md:rounded-r p-4 flex flex-col justify-between leading-normal w-full space-y-3">
                            <div>
                                <div class="flex items-center justify-between space-x-2 w-full">
                                    <p class="text-gray-900 font-semibold text-md mb-2">
                                        {{ $related->title }}
                                    </p>
                                    <p class="text-sm md:text-lg text-gray-600">
                                        {{ __('Ksh. ') . number_format($related->prize) }}
                                    </p>
                                </div>
                                <p class="text-xs text-gray-600">
                                    {{ $related->description }}
                                </p>
                            </div>
                            <div class="flex items-center space-x-1">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none" viewBox="0 0 24 24"
                                     stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                          d="M17.657 16.657L13.414 20.9a1.998 1.998 0 01-2.827 0l-4.244-4.243a8 8 0 1111.314 0z"/>
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                          d="M15 11a3 3 0 11-6 0 3 3 0 016 0z"/>
                                </svg>
                                <span>{{$related->county_name. ', '. $related->sub_county_name}}</span>
                            </div>
                        </div>
                    </div>
                </a>
            @endforeach
        @endisset
    </main>
    <script>
        const contactNo = document.getElementById('showContact');
        const hiddenBtn = document.getElementById('showContactHref');
        const contactNoDown = document.getElementById('showContactDown');
        const hiddenBtnDown = document.getElementById('hiddenBtnDown');

        const container = document.querySelector("#otherImages");
        const displayImage = container.querySelectorAll("img");

        window.addEventListener('load', () => {
            displayImage.forEach(element => {
                element.addEventListener('click', () => {
                    const itemId = element.getAttribute('src');
                    document.getElementById('displayPosition').setAttribute('src', itemId);
                });
            });
        });

        contactNo.addEventListener('click', (e) => {
            e.preventDefault();
            contactNo.classList.remove('flex');
            contactNo.classList.add('hidden');
            hiddenBtn.classList.remove('hidden');
            hiddenBtn.classList.add('flex');
        });

        contactNoDown.addEventListener('click', (e) => {
            e.preventDefault();
            contactNoDown.classList.remove('flex');
            contactNoDown.classList.add('hidden');
            hiddenBtnDown.classList.remove('hidden');
            hiddenBtnDown.classList.add('flex');
        });

        //Request callback
        const callButton = document.getElementById('callBackRequest');
        const requestForm = document.getElementById('requestCallForm');
        const closeForm = document.getElementById('closeForm');
        const submitBtn = document.getElementById('submitBtn');
        const phoneNumber = document.getElementById('phoneNumber');
        const userName = document.getElementById('userName');
        const successMessage = document.getElementById('successMessage');

        callButton.addEventListener('click', (e) => {
            e.preventDefault();
            requestForm.classList.remove('hidden');
            callButton.classList.add('hidden');
        });

        closeForm.addEventListener('click', (e) => {
            e.preventDefault();
            requestForm.classList.add('hidden');
            callButton.classList.remove('hidden');
            userName.value = '';
            phoneNumber.value = '';
        });

        submitBtn.addEventListener('click', (event) => {
            event.preventDefault();
            const formData = {
                username: userName.value,
                phone_number: phoneNumber.value,
            };

            axios.post('/call/request', {
                name: userName.value,
                phone_number: phoneNumber.value,
            }).then(function (res) {
                // console.log(res.data.data.name);
                successMessage.innerHTML = 'Dear ' + res.data.name + ' your contact no ' + res.data
                    .phone_number + ' has been sent.'
            }).catch((error) => {
                console.log(error);
            })
            // console.log(formData);
            requestForm.classList.add('hidden');
            callButton.classList.remove('hidden');
            userName.value = '';
            phoneNumber.value = '';
            successMessage.innerHTML = 'Your request has been sent successfully';
        });
    </script>
@endsection
