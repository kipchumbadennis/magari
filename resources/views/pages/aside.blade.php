<!-- sidebar -->
<div id="stickyAside"
    class=" z-20 sidebar w-3/4 sm:w-1/2 md:w-1/4 space-y-6 py-1 px-4 md:px-0 absolute inset-y-0 left-0 transform -translate-x-full md:relative md:translate-x-0 transition duration-200 ease-in-out">
    <!-- nav -->
    <nav class="text-dark bg-white rounded">
        <a href="#"
            class="flex justify-between items-center py-1 px-1 transition duration-200 hover:bg-gray-300 hover:text-white">
            <div class="flex flex-col space-y-0">
                <p>Tracktors & Heavy Equipements</p>
                <p class="text-gray-500 text-xs hover:text-white">1,500 ads</p>
            </div>
            <div class="">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                    <path fill-rule="evenodd"
                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                        clip-rule="evenodd" />
                </svg>
            </div>
        </a>
        <a href="#"
            class="flex justify-between items-center py-1 px-1 transition duration-200 hover:bg-gray-300 hover:text-white">
            <div class="flex flex-col space-y-0">
                <p>Trucks & Trailers</p>
                <p class="text-gray-500 text-xs hover:text-white">1,500 ads</p>
            </div>
            <div class="">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                    <path fill-rule="evenodd"
                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                        clip-rule="evenodd" />
                </svg>
            </div>
        </a>
        <a href="#"
            class="flex justify-between items-center py-1 px-1 transition duration-200 hover:bg-gray-300 hover:text-white">
            <div class="flex flex-col space-y-0">
                <p>Cars</p>
                <p class="text-gray-500 text-xs hover:text-white">1,500 ads</p>
            </div>
            <div class="">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                    <path fill-rule="evenodd"
                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                        clip-rule="evenodd" />
                </svg>
            </div>
        </a>
        <a href="#"
            class="flex justify-between items-center py-1 px-1 transition duration-200 hover:bg-gray-300 hover:text-white">
            <div class="flex flex-col space-y-0">
                <p>Buses & Vans</p>
                <p class="text-gray-500 text-xs hover:text-white">1,500 ads</p>
            </div>
            <div class="">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                    <path fill-rule="evenodd"
                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                        clip-rule="evenodd" />
                </svg>
            </div>
        </a>
        <a href="#"
            class="flex justify-between items-center py-1 px-1 transition duration-200 hover:bg-gray-300 hover:text-white">
            <div class="flex flex-col space-y-0">
                <p>Motorcycles & Scooters</p>
                <p class="text-gray-500 text-xs hover:text-white">1,500 ads</p>
            </div>
            <div class="">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                    <path fill-rule="evenodd"
                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                        clip-rule="evenodd" />
                </svg>
            </div>
        </a>
        <a href="#"
            class="flex justify-between items-center py-1 px-1 transition duration-200 hover:bg-gray-300 hover:text-white">
            <div class="flex flex-col space-y-0">
                <p>Parts & Accessories</p>
                <p class="text-gray-500 text-xs hover:text-white">1,500 ads</p>
            </div>
            <div class="">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                    <path fill-rule="evenodd"
                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                        clip-rule="evenodd" />
                </svg>
            </div>
        </a>
    </nav>
</div>
<script>
    // grab everything we need
    const btn = document.querySelector(".mobile-menu-button");
    const sidebar = document.querySelector(".sidebar");

    // add our event listener for the click
    btn.addEventListener("click", (e) => {
        sidebar.classList.toggle("-translate-x-full");
    });

    document.getElementById('stickyAside').addEventListener('mouseup', function(e) {
    var container = document.getElementById('stickyAside');
    if (!container.contains(e.target)) {
        container.style.display = 'none';
    }
});
</script>
