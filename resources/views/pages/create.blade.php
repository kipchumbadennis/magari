@extends('layouts.app')
@section('title', 'Add Product')
@section('content')
    <div class="py-10 px-40">
        {{-- {{$data}} --}}
        <div class="bg-white rounded p-4 relative">
            <div class=" text-center text-xl font-bold pb-4">Post Ad</div>
            <div class="flex">
                <div class="inline-flex z-10 bg-blue-400 rounded-full w-1/2 absolute left-5 py-2 px-4 cursor-pointer"
                     id="about">
                    <p class="text-white">About Ad</p>
                </div>
                <div class="inline-flex z-20 bg-orange-600 rounded-full w-1/2 absolute right-5 py-2 px-4 cursor-pointer"
                     id="details">
                    <p class="text-white">Ad Details</p>
                </div>
            </div>
            <div class="py-12 px-3">
                <form action="{{ route('post.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div id="aboutForm" class=" w-full py-4 flex justify-between space-x-5">
                        <div class="w-1/2">
                            <div class="w-full">
                                <label for="category" class="block text-xs font-semibold mb-2">Category</label>
                                <select name="category" id="category"
                                        class="block w-full border border-gray-300 rounded py-2 px-4 text-xs">
                                    <option value="" disabled selected>Select make</option>
                                    @foreach ($data as $item)
                                        <option value="{{ $item->id }}">{{ $item->make }}</option>
                                    @endforeach
                                </select>
                                @error('category')
                                <p class="text-red=800 text-xs py-1">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="w-full pt-2 hidden" id="modelDiv">
                                <select name="model" id="model"
                                        class="block w-full border border-gray-300 rounded px-4 py-2 text-xs">
                                </select>
                                @error('model')
                                <p class="text-red=800 text-xs py-1">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="pt-2 mb-4">
                                <input type="file" name="image[]" id="image"
                                       class="rounded py-2 px-2 invisible absolute" multiple>
                                <button class="bg-orange-900 py-2 px-3 text-white rounded" id="fileUpload">Upload
                                    images
                                </button>
                            </div>
                            <div class="flex space-x-2 overflow-x-auto rounded border border-blue-500 hidden"
                                 id="preview"></div>

                        </div>
                        <div class="w-1/2">
                            <div class="w-full">
                                <label for="county" class="block text-xs font-semibold mb-2">Location</label>
                                <select name="county" id="county"
                                        class="block w-full border border-gray-300 rounded px-4 py-2 text-xs">
                                    <option value="" disabled selected>Select County</option>
                                    @foreach ($counties as $county)
                                        <option value="{{ $county->id }}">{{ $county->county_name }}</option>
                                    @endforeach
                                </select>
                                @error('county')
                                <p class="text-red=800 text-xs py-1">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="w-full pt-2 hidden" id="subCountyDiv">
                                <select name="subcounty" id="subcounty"
                                        class="block w-full border border-gray-300 rounded px-4 py-2 text-xs">
                                </select>
                                @error('subcounty')
                                <p class="text-red=800 text-xs py-1">{{ $message }}</p>
                                @enderror
                            </div>
                        </div>
                        {{-- <div class="w-full">
                        <input type="file" name="image[]" id="image" class="rounded py-2 px-2" multiple>
                    </div> --}}
                    </div>
                    <div id="detailsForm" class="w-full">
                        <div class="flex space-x-5">
                            <div class="w-1/2 mb-2">
                                <input type="text" id="title" name="title"
                                       class="w-full border border-gray-400 rounded text-gray-500 py-1 px-4 placeholder:text-xs"
                                       placeholder="Title">
                                @error('title')
                                <p class="text-red=800 text-xs py-1">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="w-1/2"></div>
                        </div>


                        <div class="flex  space-x-5 mb-2">
                            <div class="w-1/2 group mb-2">
                                <?php $years = range(strftime('%Y', time()), 1700); ?>
                                <select class="w-full border border-gray-400 rounded text-gray-500 py-1 px-4 text-xs"
                                        name="year_of_manufacture" id="yearOfManufacture">
                                    <option selected disabled value="">Year of Manufacture</option>
                                    <?php foreach($years as $year) : ?>
                                    <option value="<?php echo $year; ?>"><?php echo $year; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                @error('year_of_manufacture')
                                <p class="text-red=800 text-xs py-1">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="w-1/2 group">
                                <input type="text" id="color" name="color"
                                       class="w-full border border-gray-400 rounded text-gray-500 py-1 px-4 placeholder:text-xs"
                                       placeholder="Color">
                                @error('color')
                                <p class="text-red=800 text-xs py-1">{{ $message }}</p>
                                @enderror
                            </div>
                        </div>
                        <div class="flex space-x-5 mb-2">
                            <div class="w-1/2 group mb-2">
                                <input type="text" id="mileage" name="mileage"
                                       class="w-full border border-gray-400 rounded text-gray-500 py-1 px-4 placeholder:text-xs"
                                       placeholder="Mileage (Km)">
                                @error('mileage')
                                <p class="text-red=800 text-xs py-1">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="w-1/2">
                                <div class="flex">
                                        <span
                                            class="inline-flex items-center px-3 text-xs text-gray-500 rounded-l-md border border-r-0 border-gray-400">
                                            Transmission
                                        </span>

                                    <select
                                        class="w-full border border-gray-400 text-gray-500 py-1 px-4 rounded-r text-xs"
                                        id="transmission" name="transmission">
                                        <option selected value="" disabled>Choose...</option>
                                        <option value="manual">Manual</option>
                                        <option value="auto">Automatic</option>
                                    </select>
                                </div>
                                @error('transmission')
                                <p class="text-red=800 text-xs py-1">{{ $message }}</p>
                                @enderror
                            </div>
                        </div>
                        <div class="flex space-x-5 mb-2">
                            <div class="w-1/2 mb-2">
                                <div class="group flex">
                                        <span
                                            class="inline-flex items-center px-3 text-xs text-gray-500 rounded-l-md border border-r-0 border-gray-400">
                                            Condition
                                        </span>

                                    <select
                                        class="w-full border border-gray-400 text-gray-500 py-1 px-4 rounded-r text-xs"
                                        id="condition" name="condition">
                                        <option selected value="" disabled>Choose...</option>
                                        <option value="new">New</option>
                                        <option value="kenya">Kenyan Used</option>
                                        <option value="foreign">Foreign Used</option>
                                    </select>
                                </div>
                                @error('condition')
                                <p class="text-red=800 text-xs py-1">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="w-1/2 form-froup">
                                    <textarea name="condition_description" id="conditionDesc" cols="" rows=""
                                              class="w-full border border-gray-400 rounded text-gray-500 py-1 px-4 placeholder:text-xs"
                                              placeholder="Describe the fault condition(s) of the ad"></textarea>
                                @error('condition_description')
                                <p class="text-red=800 text-xs py-1">{{ $message }}</p>
                                @enderror
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="w-1/2 flex items-center space-x-2">
                                <p class="capitalize">registered</p>
                                <div class="form-check inline-flex items-center space-x-2">
                                    <input class="form-check-input" type="radio" name="registered" id="exampleRadios1"
                                           value="2" checked>
                                    <label class="form-check-label" for="exampleRadios1">
                                        Yes
                                    </label>
                                </div>
                                <div class="form-check inline-flex items-center space-x-2">
                                    <input class="form-check-input" type="radio" name="registered" id="exampleRadios2"
                                           value="1">
                                    <label class="form-check-label" for="exampleRadios2">
                                        No
                                    </label>
                                </div>
                                @error('registered')
                                <p class="text-red=800 text-xs py-1">{{ $message }}</p>
                                @enderror

                            </div>
                        </div>
                        <div class="mb-2">
                            <div class="group">
                                <label for="description" class="mb-2">Description</label>
                                <textarea
                                    class="w-full border border-gray-400 rounded text-gray-500 py-1 px-4 placeholder:text-xs"
                                    name="description" id="description" rows="3" placeholder="Description*"></textarea>
                                @error('description')
                                <p class="text-red=800 text-xs py-1">{{ $message }}</p>
                                @enderror
                            </div>
                        </div>
                        <div class="row d-flex align-items-center mb-2">
                            <div class="w-1/2 mb-2">
                                <label for="prize" class="block mb-2 text-sm font-medium text-gray-400">Prize</label>
                                <div class="flex">
                                        <span
                                            class="inline-flex items-center px-3 py-2 text-xs text-gray-900 bg-gray-200 rounded-l-md border border-r-0 border-gray-300 dark:bg-gray-600 dark:text-gray-400 dark:border-gray-600">
                                            Ksh
                                        </span>
                                    <input type="text" id="prize" name="prize"
                                           class="rounded-none rounded-r-lg px-2 bg-gray-50 border border-gray-400 text-gray-400 block flex-1 min-w-0 w-full text-sm placeholder:text-xs"
                                           placeholder="Prize*">
                                </div>
                                @error('prize')
                                <p class="text-red=800 text-xs py-1">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="w-1/2">
                                <div class="group">
                                    <label for="negotiable" class="mb-2 px-5">Negotiable</label>
                                    <input class="form-check-input" type="checkbox" name="negotiable" id="togBtn"
                                           value="1">
                                    @error('negotiable')
                                    <p class="text-red=800 text-xs py-1">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="flex w-full space-x-5 mb-2">
                            <div class="w-1/2">
                                <div class="group w-full">
                                    <label for="phone_number">Your phone number</label>
                                    <input type="text" name="phone_number" id="phone_number"
                                           class="w-full border border-gray-400 rounded text-gray-500 py-1 px-4"
                                           value="0715738974">
                                    @error('phone_number')
                                    <p class="text-red=800 text-xs py-1">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                            <div class="w-1/2">
                                <div class="group w-full">
                                    <label for="name">Your name</label>
                                    <input type="text" name="name" id="name"
                                           class="w-full border border-gray-400 rounded text-gray-500 py-1 px-4"
                                           value="Dennis Kipchumba">
                                    @error('name')
                                    <p class="text-red=800 text-xs py-1">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="mb-2 pt-3">
                            <p class="text-center h5 font-weight-bold text-gray-900">Promote your Ad</p>
                            <h6 class="text-center text-gray-600">Please choose below packages to promote your add</h6>

                            <div class="flex flex-col space-y-3 items-center pt-5">
                                <div class="w-1/2">
                                    <div class="form-check mb-4">
                                        <label class="form-check-label cursor-pointer" for="boost">
                                            <input class="form-check-input invisible absolute" type="radio"
                                                   name="package" id="boost" value="boost" checked>
                                            <div class="rounded py-2 px-4 border border-blue-800">
                                                Boost
                                            </div>
                                        </label>
                                    </div>
                                    <div class="form-check mb-4">
                                        <label class="form-check-label cursor-pointer" for="platinum">
                                            <input class="form-check-input invisible absolute" type="radio"
                                                   name="package" id="platinum" value="platinum">
                                            <div class="rounded py-2 px-4 border border-blue-800">
                                                Platinum
                                            </div>
                                        </label>
                                    </div>
                                    <div class="form-check mb-4">
                                        <label class="form-check-label cursor-pointer" for="jumbo">
                                            <input class="form-check-input invisible absolute" type="radio"
                                                   name="package" id="jumbo" value="jumbo">
                                            <div class="rounded py-2 px-4 border border-blue-800">
                                                Jumbo
                                            </div>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="rounded py-1 px-4 text-white bg-slate-500" id=""> Post Ad</button>
                    </div>
                    <button type="submit" class="absolute bottom-5 right-2 rounded py-1 px-4 text-white bg-slate-500"
                            id="submitButton"> Post Ad
                    </button>
                </form>
            </div>
            <button class="absolute bottom-2 right-2 bg-orange-400 rounded py-1 px-4 text-white" id="nextBtn">Next
            </button>
            <button class="absolute bottom-2 right-2 bg-orange-400 rounded py-1 px-4 text-white hidden"
                    id="prevBtn">Previous
            </button>
        </div>
    </div>
    <script>
        window.addEventListener('load', function () {
            document.getElementById('fileUpload').addEventListener('click', (element) => {
                element.preventDefault();
                document.getElementById('image').click();
            });
            const btn = document.getElementById('submitButton');
            btn.addEventListener('click', function (event) {
                event.preventDefault();
                const config = {
                    headers: {
                        'content-type': 'multipart/form-data'
                    }
                }

                const formData = {
                    make: document.getElementById('category').value,
                    model: document.getElementById('model').value,
                    county: document.getElementById('county').value,
                    subcounty: document.getElementById('subcounty').value,
                    title: document.getElementById('title').value,
                    yom: document.getElementById('yearOfManufacture').value,
                    color: document.getElementById('color').value,
                    mileage: document.getElementById('mileage').value,
                    transmission: document.getElementById('transmission').value,
                    condition: document.getElementById('condition').value,
                    conditionDesc: document.getElementById('conditionDesc').value,
                    registered: document.querySelector('input[name=registered]:checked').value,
                    prize: document.getElementById('prize').value,
                    neg: document.getElementById('togBtn').value,
                    phone_number: document.getElementById('phone_number').value,
                    name: document.getElementById('name').value,
                    package: document.getElementById('togBtn').checked,
                }

                axios.post('/Post/Ad', formData, config).then(function (res) {
                    console.log(res);
                }).catch(function (error) {
                    console.log(error);
                });
            })
        });

        document.getElementById('image').addEventListener("change", previewImages);

        function previewImages() {

            const preview = document.querySelector('#preview');

            if (this.files) {
                [].forEach.call(this.files, readAndPreview);
            }

            function readAndPreview(file) {

                // Make sure `file.name` matches our extensions criteria
                if (!/\.(jpe?g|png|gif|jfif)$/i.test(file.name)) {
                    return alert(file.name + " is not an image");
                } // else...

                var reader = new FileReader();

                reader.addEventListener("load", function () {
                    var image = new Image();
                    image.classList.add('h-16');
                    image.classList.add('rounded');
                    preview.classList.remove('hidden');
                    image.title = file.name;
                    image.src = this.result;
                    preview.appendChild(image);
                });

                reader.readAsDataURL(file);

            }

        }

        document.getElementById('togBtn').addEventListener('click', () => {
            var togBtn = document.getElementById('togBtn');

            if (togBtn.checked == true) {
                togBtn.setAttribute('value', '2');
            } else {
                togBtn.setAttribute('value', '1');
            }
        });
    </script>
@endsection
