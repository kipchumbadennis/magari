@extends('layouts.app')
@section('content')
    <div class="flex-col space-y-3 md:space-y-0 md:space-x-5 md:flex-row flex w-full px-2 sm:px-6 md:px-40">
        <div class="md:w-1/4 bg-white rounded" id="filterSearch">
            <div class="space-y-6 flex-column p-2">
                <div>
                    <p class="font-semibold">Filter Results</p>
                    <div class="">
                        <input type="text" class="rounded w-full border border-gray-200 p-2 text-xs"
                               value="@isset($searched){{$searched}}@endisset" placeholder="Search..." name="searchable"
                               id="searchable">
                    </div>
                </div>
                <div>
                    <label for="sort" class="font-semibold text-xs">Sort by</label>
                    <select name="sort" id="sort" class="w-full text-xs p-2 mb-2 rounded border border-gray-200">
                        <option value="" disabled selected
                                class="rounded border border-gray-200 py-1 px-2 text-xs py-1 px-2">Relevance
                        </option>
                        <option value="location" class="rounded border border-gray-200 py-1 px-2 text-xs py-1 px-2">
                            Location
                        </option>
                        <option value="make" class="rounded border border-gray-200 py-1 px-2 text-xs py-1 px-2">Make
                        </option>
                    </select>

                    <select name="location" id="selectLocation"
                            class="hidden w-full py-2 rounded text-xs px-2 border border-gray-200">
                        @isset($sortlocation)
                            <option value="all" class="rounded border border-gray-200 py-1 px-2 text-xs py-1 px-2"
                                    selected disabled>Choose location
                            </option>
                            @foreach($sortlocation as $locations)
                                <option value="{{$locations->id}}"
                                        class="rounded border border-gray-200 py-1 px-2 text-sm">{{$locations->county_name}}</option>
                            @endforeach
                        @else
                            <option value="" class="rounded border border-gray-200 py-1 px-2 text-xs py-1 px-2"
                                    selected disabled>No locations
                            </option>
                        @endisset
                    </select>

                    <select name="make" id="selectMake"
                            class="hidden w-full py-2 rounded text-xs px-2 border border-gray-200">
                        @isset($makes)
                            <option value="all" class="rounded border border-gray-200 py-1 px-2 text-xs py-1 px-2"
                                    selected
                                    disabled>
                                Choose makes
                            </option>
                            @foreach($makes as $make)
                                <option value="{{$make->id}}"
                                        class="rounded border border-gray-200 py-1 px-2 text-sm">{{$make->make}}</option>
                            @endforeach
                        @else
                            <option value="" class="rounded border border-gray-200 py-1 px-2 text-xs py-1 px-2" selected
                                    disabled>
                                No makes
                            </option>
                        @endisset
                    </select>
                </div>
                {{--                <div>--}}
                {{--                    <p class="capitalize">Mileage</p>--}}
                {{--                    <div>--}}
                {{--                        <div class="form-check items-center space-x-2">--}}
                {{--                            <input class="form-check-input" type="radio" name="registered" id="exampleRadios1"--}}
                {{--                                   value="2" checked>--}}
                {{--                            <label class="form-check-label" for="exampleRadios1">--}}
                {{--                                Yes--}}
                {{--                            </label>--}}
                {{--                        </div>--}}
                {{--                        <div class="form-check items-center space-x-2">--}}
                {{--                            <input class="form-check-input" type="radio" name="registered" id="exampleRadios1"--}}
                {{--                                   value="2">--}}
                {{--                            <label class="form-check-label" for="exampleRadios1">--}}
                {{--                                Yes--}}
                {{--                            </label>--}}
                {{--                        </div>--}}
                {{--                        <div class="form-check items-center space-x-2">--}}
                {{--                            <input class="form-check-input" type="radio" name="registered" id="exampleRadios1"--}}
                {{--                                   value="2">--}}
                {{--                            <label class="form-check-label" for="exampleRadios1">--}}
                {{--                                Yes--}}
                {{--                            </label>--}}
                {{--                        </div>--}}
                {{--                        <div class="form-check items-center space-x-2">--}}
                {{--                            <input class="form-check-input" type="radio" name="registered" id="exampleRadios1"--}}
                {{--                                   value="2">--}}
                {{--                            <label class="form-check-label" for="exampleRadios1">--}}
                {{--                                Yes--}}
                {{--                            </label>--}}
                {{--                        </div>--}}
                {{--                        <div class="form-check items-center space-x-2">--}}
                {{--                            <input class="form-check-input" type="radio" name="registered" id="exampleRadios1"--}}
                {{--                                   value="2">--}}
                {{--                            <label class="form-check-label" for="exampleRadios1">--}}
                {{--                                Yes--}}
                {{--                            </label>--}}
                {{--                        </div>--}}
                {{--                        <div class="form-check items-center space-x-2">--}}
                {{--                            <input class="form-check-input" type="radio" name="registered" id="exampleRadios1"--}}
                {{--                                   value="2">--}}
                {{--                            <label class="form-check-label" for="exampleRadios1">--}}
                {{--                                Yes--}}
                {{--                            </label>--}}
                {{--                        </div>--}}
                {{--                    </div>--}}
                {{--                </div>--}}
                <div>
                    <p class="capitalize font-semibold text-xs">Ad Condition</p>
                    <div class="text-sm">
                        <input class="form-check-input hidden" type="radio" name="condition" id="all" value="all"
                               checked>
                        <div class="form-check items-center space-x-2">
                            <input class="form-check-input" type="radio" name="condition" id="new"
                                   value="new">
                            <label class="form-check-label" for="new">
                                New
                            </label>
                        </div>
                        <div class="form-check items-center space-x-2">
                            <input class="form-check-input" type="radio" name="condition" id="kenya"
                                   value="kenya">
                            <label class="form-check-label" for="kenya">
                                Used in Kenya
                            </label>
                        </div>
                        <div class="form-check items-center space-x-2">
                            <input class="form-check-input" type="radio" name="condition" id="foreign"
                                   value="foreign">
                            <label class="form-check-label" for="foreign">
                                Used Abroad
                            </label>
                        </div>
                    </div>
                </div>
                <div>
                    <p class="capitalize font-semibold text-xs">Ad Prize</p>
                    <div class="text-sm">
                        <input class="form-check-input hidden" checked type="radio" name="prize" id="all" value="all">
                        <div class="form-check items-center space-x-2">
                            <input class="form-check-input" type="radio" name="prize" id="below50K"
                                   value="below50K">
                            <label class="form-check-label" for="below50K">
                                Below {{number_format(50).' K'}}
                            </label>
                        </div>
                        <div class="form-check items-center space-x-2">
                            <input class="form-check-input" type="radio" name="prize" id="between50and100"
                                   value="between50and100">
                            <label class="form-check-label" for="between50and100">
                                Between {{number_format(500)}} and {{number_format(100). ' K'}}
                            </label>
                        </div>
                        <div class="form-check items-center space-x-2">
                            <input class="form-check-input" type="radio" name="prize" id="between100and500"
                                   value="between100and500">
                            <label class="form-check-label" for="between100and500">
                                Between {{number_format(100)}} and {{number_format(500). ' K'}}
                            </label>
                        </div>
                        <div class="form-check items-center space-x-2">
                            <input class="form-check-input" type="radio" name="prize" id="between500and999"
                                   value="between500and999">
                            <label class="form-check-label" for="between500and999">
                                Between {{number_format(500).' K'}} and {{number_format(1). ' M'}}
                            </label>
                        </div>
                        <div class="form-check items-center space-x-2">
                            <input class="form-check-input" type="radio" name="prize" id="above1K"
                                   value="above1M">
                            <label class="form-check-label" for="above1K">
                                Above {{number_format(1). ' M'}}
                            </label>
                        </div>
                    </div>
                </div>
                <div>
                    <p class="capitalize font-semibold text-xs">Registered</p>
                    <div class="text-sm">
                        <input class="form-check-input hidden" checked type="radio" name="registered" id="all"
                               value="all">
                        <div class="form-check items-center space-x-2">
                            <input class="form-check-input" type="radio" name="registered" id="2"
                                   value="2">
                            <label class="form-check-label" for="2">
                                Yes
                            </label>
                        </div>
                        <div class="form-check items-center space-x-2">
                            <input class="form-check-input" type="radio" name="registered" id="1"
                                   value="1">
                            <label class="form-check-label" for="1">
                                No
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="md:w-3/4" id="filteredItems">
            <div class="">
                <p class="p-2">Filtered Items</p>
                <div class="px-2">
                    <a href="#" id="searchRef"
                       class="inline-flex relative items-center py-1 px-4 rounded border border-red-400 text-xs">
                        {{$searched}}
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 ml-3" fill="none" viewBox="0 0 24 24"
                             stroke="currentColor" stroke-width="2" id="clearSearch">
                            <path stroke-linecap="round" stroke-linejoin="round" d="M6 18L18 6M6 6l12 12"/>
                        </svg>
                    </a>
                </div>
                <div id="filteredData" class="space-y-4">
                    <div class="p-2 space-y-4" id="appendItems">
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <script>
        window.addEventListener('load', performFilter);
        let clearSearch = document.getElementById('clearSearch');
        let sortValue = document.getElementById('sort');
        let locationSelect = document.getElementById('selectLocation');
        let selectMake = document.getElementById('selectMake');
        clearSearch.addEventListener('click', (event) => {
            event.preventDefault();
            document.getElementById('searchable').setAttribute('value', '');
        });

        sortValue.addEventListener('change', () => {
            if (sortValue.value === 'location') {
                locationSelect.classList.remove('hidden');
                selectMake.classList.add('hidden');
            } else if (sortValue.value === 'make') {
                selectMake.classList.remove('hidden');
                locationSelect.classList.add('hidden');
            }
        });
        locationSelect.addEventListener("change", performFilter);
        selectMake.addEventListener('change', performFilter);

        let conditionValue = document.querySelectorAll('input[type=radio][name=condition]');
        conditionValue.forEach(radio => radio.addEventListener('change', () => {
            let adCondition = radio.value;
            performFilter();
        }));

        let prizeValue = document.querySelectorAll('input[type=radio][name=prize]');
        prizeValue.forEach(prize => prize.addEventListener('change', () => {
            let prizeRange = prize.value;
            performFilter();
        }));

        let registeredAd = document.querySelectorAll('input[type=radio][name=registered]');
        registeredAd.forEach(item => item.addEventListener('change', () => {
            let registeredValue = item.value;
            performFilter();
        }));


        function performFilter() {
            const searchKey = document.getElementById('searchable').value;
            let locationId = locationSelect.value;
            let makeId = selectMake.value;
            let adCondition = document.querySelector('input[name=condition]:checked').value;
            let prizeRange = document.querySelector('input[name=prize]:checked').value;
            let registeredValue = document.querySelector('input[name=registered]:checked').value;

            const formData = ({
                search: searchKey,
                location: locationId,
                make: makeId,
                condition: adCondition,
                prize: prizeRange,
                registered: registeredValue,
            });
            axios.post('/searches', formData).then(function (response) {
                console.log(response.data);
                $('#appendItems').empty();
                let returnResponse = response.data.searched;
                returnResponse.forEach(item => {
                    let rawUrl = "{{route('ad.show', ['make' =>":make", 'model' => ':model', 'slug' => ':slug'])}}";
                    // const url = rawUrl.replace(':make', item.make_ref, ':model', item.model_ref, ':slug', item.slug);
                    const url = rawUrl.replace(':make', item.make_ref).replace(':model', item.model_ref).replace(':slug', item.slug);
                    console.log(url);
                    $('#appendItems').append(
                        '<a href="'+url+'" class="flex flex-col bg-white rounded-lg border shadow-md md:flex-row shadow">'
                        + '<img class="object-cover rounded-t-lg md:h-50 h-auto md:w-1/4 md:rounded-none md:rounded-l-lg" src="/vehicles/' + item.shown + '" alt="">'
                        + '<div class="flex flex-col justify-between p-4 leading-normal md:w-3/4">'
                        + '<h5 class="mb-2 text-sm font-bold tracking-tight">' + item.title + '</h5>'
                        + '<p class="mb-3 text-xs font-normal text-gray-700 dark:text-gray-400">' + item.description + '</p>'
                        + '</div>'
                        + '</a>');
                });
                document.getElementById('searchRef').innerText = returnResponse.search;
            }).catch(function (error) {
                console.log(error);
            });
            // return formData;
        }
    </script>
@endsection
