const { default: axios } = require('axios');

require('./bootstrap');

var bodyPage = document.getElementById('app');
const loader = document.getElementById('loader');

window.addEventListener('load', function () {
    loader.classList.add('hidden');
    loader.classList.add('transition');
    loader.classList.add('ease-in-out');
    loader.classList.add('duration-200');
    bodyPage.classList.remove('hidden');
    bodyPage.classList.add('transition');
    bodyPage.classList.add('ease-in-out');
    bodyPage.classList.add('duration-200');

    $('#category').on('change', function () {

        if (!$('#category').val() == '') {
            categoryId = $(this).val();
            href = '/show/' + categoryId;

            axios.get(href).then(function (res) {
                if (res.data) {
                    $('#modelDiv').removeClass('hidden');
                    $('#model').empty();
                    $('#model').append('<option hidden disabled>Choose model</option>');
                    res.data.forEach(model => {
                        $('select[name="model"]').append('<option value="' + model.id + '">' + model.model + '</option>');
                    });

                } else {
                    $('#model').empty();
                    $('#modelDiv').addClass('hidden');
                }
            }).catch(function (error) {
                console.log(error);
            })
        } else {
            $('#modelDiv').addClass('hidden');
            $('#model').empty();
        }
    });

    $('#county').on('change', function () {
        let countyId = '';
        let href = '';
        if (!$('#county').val() == '') {
            countyId = $(this).val();

            href = '/subcounty/' + countyId;

            axios.get(href).then(function (response) {
                $('#subCountyDiv').removeClass('hidden');
                $('#subcounty').empty();
                response.data.forEach(subcounty => {
                    $('select[name="subcounty"]').append('<option value="' + subcounty.id + '">' + subcounty.sub_county_name + '</option>');
                });

            }).catch(function (error) {
                console.log(error);
            });
        } else {
            $('#subcounty').empty();
        }
    });
});
