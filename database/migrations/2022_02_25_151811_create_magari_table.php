<?php

use App\Models\AdMake;
use App\Models\Subsidiary;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('magari', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->foreignIdFor(AdMake::class);
            $table->foreignIdFor(Subsidiary::class);
            $table->year('year_of_manufacture');
            $table->string('color');
            $table->string('mileage');
            $table->enum('condition', ['new', 'kenya', 'foreign'])->default('kenya');
            $table->string('sec_condition')->nullable();
            $table->enum('registered', [1,2])->nullable()->default(1);
            $table->longText('description')->nullable();
            $table->float('prize', 12, 2);
            $table->enum('negotiable', [1,2])->nullable()->default(1);
            $table->longText('images');
            $table->string('shown');
            $table->string('county_id');
            $table->string('sub_county_id');
            $table->string('transmission');
            $table->string('phone_number');
            $table->string('name');
            $table->string('package');
            $table->string('slug');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('magari');
    }
};
