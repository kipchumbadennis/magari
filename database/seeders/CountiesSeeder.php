<?php

namespace Database\Seeders;

use App\Models\Counties;
use Illuminate\Database\Seeder;

class CountiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Counties::truncate();

        $counties = [
            [
                'county_name' => 'Mombasa'
            ],
            [
                'county_name' => 'Kwale'
            ],
            [
                'county_name' => 'Kilifi'
            ],
            [
                'county_name' => 'Tana River'
            ],
            [
                'county_name' => 'Lamu'
            ],
            [
                'county_name' => 'Taita Taveta'
            ],
            [
                'county_name' => 'Garissa'
            ],
            [
                'county_name' => 'Wajir'
            ],
            [
                'county_name' => 'Mandera'
            ],
            [
                'county_name' => 'Marsabit'
            ],
            [
                'county_name' => 'Isiolo'
            ],
            [
                'county_name' => 'Meru'
            ],
            [
                'county_name' => 'Tharaka Nithi'
            ],
            [
                'county_name' => 'Embu'
            ],
            [
                'county_name' => 'Kitui'
            ],
            [
                'county_name' => 'Machakos'
            ],
            [
                'county_name' => 'Makueni'
            ],
            [
                'county_name' => 'Nyandarua'
            ],
            [
                'county_name' => 'Nyeri'
            ],
            [
                'county_name' => 'Kirinyaga'
            ],
            [
                'county_name' => "Murang'a"
            ],
            [
                'county_name' => 'Kiambu'
            ],
            [
                'county_name' => 'Turkana'
            ],
            [
                'county_name' => 'West Pokot'
            ],
            [
                'county_name' => 'Samburu'
            ],
            [
                'county_name' => 'Trans Nzoia'
            ],
            [
                'county_name' => 'Uasin Gishu'
            ],
            [
                'county_name' => 'Elgeyo Marakwet'
            ],
            [
                'county_name' => 'Nandi'
            ],
            [
                'county_name' => 'Baringo'
            ],
            [
                'county_name' => 'Laikipia'
            ],
            [
                'county_name' => 'Nakuru'
            ],
            [
                'county_name' => 'Narok'
            ],
            [
                'county_name' => 'Kajiado'
            ],
            [
                'county_name' => 'Kericho'
            ],
            [
                'county_name' => 'Bomet'
            ],
            [
                'county_name' => 'Kakamega'
            ],
            [
                'county_name' => 'Vihiga'
            ],
            [
                'county_name' => 'Bungoma'
            ],
            [
                'county_name' => 'Busia'
            ],
            [
                'county_name' => 'Siaya'
            ],
            [
                'county_name' => 'Kisumu'
            ],
            [
                'county_name' => 'Homa Bay'
            ],
            [
                'county_name' => 'Migori'
            ],
            [
                'county_name' => 'Kisii'
            ],
            [
                'county_name' => 'Nyamira'
            ],
            [
                'county_name' => 'Nairobi'
            ]
        ];

        foreach ($counties as $county) {
            Counties::create([
                'county_name' => $county['county_name']
            ]);
        }
    }
}
