<?php

namespace Database\Seeders;

use App\Models\SubCounties;
use Illuminate\Database\Seeder;

class SubCountiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        SubCounties::truncate();

        $sub_counties = [
            [
                'county_id' => 1,
                'sub_county_name' => 'Changamwe'
            ],
            [
                'county_id' => 1,
                'sub_county_name' => 'Jomvu'
            ],
            [
                'county_id' => 1,
                'sub_county_name' => 'Kisauni'
            ],
            [
                'county_id' => 1,
                'sub_county_name' => 'Likoni'
            ],
            [
                'county_id' => 1,
                'sub_county_name' => 'Mvita'
            ],
            [
                'county_id' => 1,
                'sub_county_name' => 'Nyali'
            ],
            [
                'county_id' => 2,
                'sub_county_name' => 'Kinango'
            ],
            [
                'county_id' => 2,
                'sub_county_name' => 'Lungalunga'
            ],
            [
                'county_id' => 2,
                'sub_county_name' => 'Matuga'
            ],
            [
                'county_id' => 2,
                'sub_county_name' => 'Msambweni'
            ],
            [
                'county_id' => 2,
                'sub_county_name' => 'Samburu Kwale'
            ],
            [
                'county_id' => 3,
                'sub_county_name' => 'Chonyi'
            ],
            [
                'county_id' => 3,
                'sub_county_name' => 'Ganze'
            ],
            [
                'county_id' => 3,
                'sub_county_name' => 'Kaloleni'
            ],
            [
                'county_id' => 3,
                'sub_county_name' => 'Kauma'
            ],
            [
                'county_id' => 3,
                'sub_county_name' => 'Kilifi North'
            ],
            [
                'county_id' => 3,
                'sub_county_name' => 'Kilifi South'
            ],
            [
                'county_id' => 3,
                'sub_county_name' => 'Magarini'
            ],
            [
                'county_id' => 3,
                'sub_county_name' => 'Malindi'
            ],
            [
                'county_id' => 3,
                'sub_county_name' => 'Rabai'
            ],
            [
                'county_id' => 4,
                'sub_county_name' => 'Tana North'
            ],
            [
                'county_id' => 4,
                'sub_county_name' => 'Tana Delta'
            ],
            [
                'county_id' => 4,
                'sub_county_name' => 'Tana River'
            ],
            [
                'county_id' => 5,
                'sub_county_name' => 'Lamu East'
            ],
            [
                'county_id' => 5,
                'sub_county_name' => 'Lamu West'
            ],
            [
                'county_id' => 6,
                'sub_county_name' => 'Mwatate'
            ],
            [
                'county_id' => 6,
                'sub_county_name' => 'Taita'
            ],
            [
                'county_id' => 6,
                'sub_county_name' => 'Taveta'
            ],
            [
                'county_id' => 6,
                'sub_county_name' => 'Voi'
            ],
            [
                'county_id' => 7,
                'sub_county_name' => 'Balambala'
            ],
            [
                'county_id' => 7,
                'sub_county_name' => 'Dadaab'
            ],
            [
                'county_id' => 7,
                'sub_county_name' => 'Fafi'
            ],
            [
                'county_id' => 7,
                'sub_county_name' => 'Garissa'
            ],
            [
                'county_id' => 7,
                'sub_county_name' => 'Halugho'
            ],
            [
                'county_id' => 7,
                'sub_county_name' => 'Ijara'
            ],
            [
                'county_id' => 7,
                'sub_county_name' => 'Lagdera'
            ],
            [
                'county_id' => 8,
                'sub_county_name' => 'Buna'
            ],
            [
                'county_id' => 8,
                'sub_county_name' => 'Eldas'
            ],
            [
                'county_id' => 8,
                'sub_county_name' => 'Habaswein'
            ],
            [
                'county_id' => 8,
                'sub_county_name' => 'Tarbaj'
            ],
            [
                'county_id' => 8,
                'sub_county_name' => 'Wajir East'
            ],
            [
                'county_id' => 8,
                'sub_county_name' => 'Wajir North'
            ],
            [
                'county_id' => 8,
                'sub_county_name' => 'Wajir South'
            ],
            [
                'county_id' => 8,
                'sub_county_name' => 'Wajir West'
            ],
            [
                'county_id' => 9,
                'sub_county_name' => 'Mandera West'
            ],
            [
                'county_id' => 9,
                'sub_county_name' => 'Banisa'
            ],
            [
                'county_id' => 9,
                'sub_county_name' => 'Katulo'
            ],
            [
                'county_id' => 9,
                'sub_county_name' => 'Lafey'
            ],
            [
                'county_id' => 9,
                'sub_county_name' => 'Mandera Central'
            ],
            [
                'county_id' => 9,
                'sub_county_name' => 'Mandera East'
            ],
            [
                'county_id' => 9,
                'sub_county_name' => 'Mandera North'
            ],
            [
                'county_id' => 10,
                'sub_county_name' => 'Loiyangalani'
            ],
            [
                'county_id' => 10,
                'sub_county_name' => 'Marsabit Central'
            ],
            [
                'county_id' => 10,
                'sub_county_name' => 'Marsabit North'
            ],
            [
                'county_id' => 10,
                'sub_county_name' => 'Marsabit South'
            ],
            [
                'county_id' => 10,
                'sub_county_name' => 'Moyale'
            ],
            [
                'county_id' => 10,
                'sub_county_name' => 'North Horr'
            ],
            [
                'county_id' => 10,
                'sub_county_name' => 'Sololo'
            ],
            [
                'county_id' => 11,
                'sub_county_name' => 'Garbatulla'
            ],
            [
                'county_id' => 11,
                'sub_county_name' => 'Isiolo'
            ],
            [
                'county_id' => 11,
                'sub_county_name' => 'Merti'
            ],
            [
                'county_id' => 12,
                'sub_county_name' => 'Buuri East'
            ],
            [
                'county_id' => 12,
                'sub_county_name' => 'Buuri West'
            ],
            [
                'county_id' => 12,
                'sub_county_name' => 'Igembe Central'
            ],
            [
                'county_id' => 12,
                'sub_county_name' => 'Igembe North'
            ],
            [
                'county_id' => 12,
                'sub_county_name' => 'Igembe South'
            ],
            [
                'county_id' => 12,
                'sub_county_name' => 'Imenti North'
            ],
            [
                'county_id' => 12,
                'sub_county_name' => 'Imenti South'
            ],
            [
                'county_id' => 12,
                'sub_county_name' => 'Meru Central'
            ],
            [
                'county_id' => 12,
                'sub_county_name' => 'Tigania Central'
            ],
            [
                'county_id' => 12,
                'sub_county_name' => 'Tigania East'
            ],
            [
                'county_id' => 12,
                'sub_county_name' => 'Tigania West'
            ],
            [
                'county_id' => 12,
                'sub_county_name' => 'Meru National Park'
            ],
            [
                'county_id' => 12,
                'sub_county_name' => 'Mt. Kenya Forest'
            ],
            [
                'county_id' => 13,
                'sub_county_name' => "Igambang'ombe"
            ],
            [
                'county_id' => 13,
                'sub_county_name' => 'Maara'
            ],
            [
                'county_id' => 13,
                'sub_county_name' => 'Meru South'
            ],
            [
                'county_id' => 13,
                'sub_county_name' => 'Tharaka North'
            ],
            [
                'county_id' => 13,
                'sub_county_name' => 'Tharaka South'
            ],
            [
                'county_id' => 13,
                'sub_county_name' => 'Mount Kenya Forest'
            ],
            [
                'county_id' => 14,
                'sub_county_name' => 'Embu East'
            ],
            [
                'county_id' => 14,
                'sub_county_name' => 'Embu North'
            ],
            [
                'county_id' => 14,
                'sub_county_name' => 'Embu West'
            ],
            [
                'county_id' => 14,
                'sub_county_name' => 'Mbeere South'
            ],
            [
                'county_id' => 14,
                'sub_county_name' => 'Mbeere North'
            ],
            [
                'county_id' => 14,
                'sub_county_name' => 'Mt. Kenya Forest'
            ],
            [
                'county_id' => 15,
                'sub_county_name' => 'Ikutha'
            ],
            [
                'county_id' => 15,
                'sub_county_name' => 'Katulani'
            ],
            [
                'county_id' => 15,
                'sub_county_name' => 'Kisasi'
            ],
            [
                'county_id' => 15,
                'sub_county_name' => 'Kitui Central'
            ],
            [
                'county_id' => 15,
                'sub_county_name' => 'Kitui West'
            ],
            [
                'county_id' => 15,
                'sub_county_name' => 'Kyuso'
            ],
            [
                'county_id' => 15,
                'sub_county_name' => 'Lower Yatta'
            ],
            [
                'county_id' => 15,
                'sub_county_name' => 'Matinyani'
            ],
            [
                'county_id' => 15,
                'sub_county_name' => 'Migwani'
            ],
            [
                'county_id' => 15,
                'sub_county_name' => 'Mumoni'
            ],
            [
                'county_id' => 15,
                'sub_county_name' => 'Mutitu'
            ],
            [
                'county_id' => 15,
                'sub_county_name' => 'Mutitu North'
            ],
            [
                'county_id' => 15,
                'sub_county_name' => 'Mutomo'
            ],
            [
                'county_id' => 15,
                'sub_county_name' => 'Mwingi Central'
            ],
            [
                'county_id' => 15,
                'sub_county_name' => 'Mwingi East'
            ],
            [
                'county_id' => 15,
                'sub_county_name' => 'Nzambani'
            ],
            [
                'county_id' => 15,
                'sub_county_name' => 'Thagicu'
            ],
            [
                'county_id' => 15,
                'sub_county_name' => 'Tseikuru'
            ],
            [
                'county_id' => 16,
                'sub_county_name' => 'Athi River'
            ],
            [
                'county_id' => 16,
                'sub_county_name' => 'Kalama'
            ],
            [
                'county_id' => 16,
                'sub_county_name' => 'Kagundo'
            ],
            [
                'county_id' => 16,
                'sub_county_name' => 'Kathiani'
            ],
            [
                'county_id' => 16,
                'sub_county_name' => 'Machakos'
            ],
            [
                'county_id' => 16,
                'sub_county_name' => 'Masinga'
            ],
            [
                'county_id' => 16,
                'sub_county_name' => 'Matungulu'
            ],
            [
                'county_id' => 16,
                'sub_county_name' => 'Mwala'
            ],
            [
                'county_id' => 16,
                'sub_county_name' => 'Yatta'
            ],
            [
                'county_id' => 17,
                'sub_county_name' => 'Kathonzweni'
            ],
            [
                'county_id' => 17,
                'sub_county_name' => 'Kibwezi'
            ],
            [
                'county_id' => 17,
                'sub_county_name' => 'Kilungu'
            ],
            [
                'county_id' => 17,
                'sub_county_name' => 'Makindu'
            ],
            [
                'county_id' => 17,
                'sub_county_name' => 'Makueni'
            ],
            [
                'county_id' => 17,
                'sub_county_name' => 'Mbooni East'
            ],
            [
                'county_id' => 17,
                'sub_county_name' => 'Mbooni West'
            ],
            [
                'county_id' => 17,
                'sub_county_name' => 'Mukaa'
            ],
            [
                'county_id' => 17,
                'sub_county_name' => 'Nzaui'
            ],
            [
                'county_id' => 18,
                'sub_county_name' => 'Kinangop'
            ],
            [
                'county_id' => 18,
                'sub_county_name' => 'Nyandarua South'
            ],
            [
                'county_id' => 18,
                'sub_county_name' => 'Mirangine'
            ],
            [
                'county_id' => 18,
                'sub_county_name' => 'Kipipiri'
            ],
            [
                'county_id' => 18,
                'sub_county_name' => 'Nyandarua Central'
            ],
            [
                'county_id' => 18,
                'sub_county_name' => 'Nyandarua West'
            ],
            [
                'county_id' => 18,
                'sub_county_name' => 'Nyandarua North'
            ],
            [
                'county_id' => 18,
                'sub_county_name' => 'Abadare National Park'
            ],
            [
                'county_id' => 19,
                'sub_county_name' => 'Tetu'
            ],
            [
                'county_id' => 19,
                'sub_county_name' => 'Kieni East'
            ],
            [
                'county_id' => 19,
                'sub_county_name' => 'Kieni West'
            ],
            [
                'county_id' => 19,
                'sub_county_name' => 'Mathira East'
            ],
            [
                'county_id' => 19,
                'sub_county_name' => 'Mathira West'
            ],
            [
                'county_id' => 19,
                'sub_county_name' => 'Nyeri South'
            ],
            [
                'county_id' => 19,
                'sub_county_name' => 'Mukurwe ini'
            ],
            [
                'county_id' => 19,
                'sub_county_name' => 'Nyeri Central'
            ],
            [
                'county_id' => 19,
                'sub_county_name' => 'Mt. Kenya Central'
            ],
            [
                'county_id' => 19,
                'sub_county_name' => 'Abardare Forest'
            ],
            [
                'county_id' => 20,
                'sub_county_name' => 'Kirinyaga Central'
            ],
            [
                'county_id' => 20,
                'sub_county_name' => 'Kirinyaga East'
            ],
            [
                'county_id' => 20,
                'sub_county_name' => 'Kirinyaga West'
            ],
            [
                'county_id' => 20,
                'sub_county_name' => 'Mwea East'
            ],
            [
                'county_id' => 20,
                'sub_county_name' => 'Mwea West'
            ],
            [
                'county_id' => 20,
                'sub_county_name' => 'Mt Kenya Forest'
            ],
            [
                'county_id' => 21,
                'sub_county_name' => "Murang'a East"
            ],
            [
                'county_id' => 21,
                'sub_county_name' => 'Kangema'
            ],
            [
                'county_id' => 21,
                'sub_county_name' => 'Mathioya'
            ],
            [
                'county_id' => 21,
                'sub_county_name' => 'Kahuro'
            ],
            [
                'county_id' => 21,
                'sub_county_name' => "Murang'a South"
            ],
            [
                'county_id' => 21,
                'sub_county_name' => 'Gatanga'
            ],
            [
                'county_id' => 21,
                'sub_county_name' => 'Kigumo'
            ],
            [
                'county_id' => 21,
                'sub_county_name' => 'Kandara'
            ],
            [
                'county_id' => 21,
                'sub_county_name' => 'Abadare Forest'
            ],
            [
                'county_id' => 22,
                'sub_county_name' => 'Gatundu North'
            ],
            [
                'county_id' => 22,
                'sub_county_name' => 'Gatundu South'
            ],
            [
                'county_id' => 22,
                'sub_county_name' => 'Gathunguri'
            ],
            [
                'county_id' => 22,
                'sub_county_name' => 'Juja'
            ],
            [
                'county_id' => 22,
                'sub_county_name' => 'Kabete'
            ],
            [
                'county_id' => 22,
                'sub_county_name' => 'Kiambaa'
            ],
            [
                'county_id' => 22,
                'sub_county_name' => 'Kiambu'
            ],
            [
                'county_id' => 22,
                'sub_county_name' => 'Kikuyu'
            ],
            [
                'county_id' => 22,
                'sub_county_name' => 'Lari'
            ],
            [
                'county_id' => 22,
                'sub_county_name' => 'Limuru'
            ],
            [
                'county_id' => 22,
                'sub_county_name' => 'Ruiru'
            ],
            [
                'county_id' => 22,
                'sub_county_name' => 'Thika East'
            ],
            [
                'county_id' => 22,
                'sub_county_name' => 'Thika West'
            ],
            [
                'county_id' => 23,
                'sub_county_name' => 'Kibish'
            ],
            [
                'county_id' => 23,
                'sub_county_name' => 'Loima'
            ],
            [
                'county_id' => 23,
                'sub_county_name' => 'Turkana Central'
            ],
            [
                'county_id' => 23,
                'sub_county_name' => 'Turkana East'
            ],
            [
                'county_id' => 23,
                'sub_county_name' => 'Turkana North'
            ],
            [
                'county_id' => 23,
                'sub_county_name' => 'Turkana South'
            ],
            [
                'county_id' => 23,
                'sub_county_name' => 'Turkana West'
            ],
            [
                'county_id' => 24,
                'sub_county_name' => 'Kipkomo'
            ],
            [
                'county_id' => 24,
                'sub_county_name' => 'Pokot Central'
            ],
            [
                'county_id' => 24,
                'sub_county_name' => 'Pokot North'
            ],
            [
                'county_id' => 24,
                'sub_county_name' => 'Pokot South'
            ],
            [
                'county_id' => 24,
                'sub_county_name' => 'West Pokot'
            ],
            [
                'county_id' => 25,
                'sub_county_name' => 'Samburu Central'
            ],
            [
                'county_id' => 25,
                'sub_county_name' => 'Samburu East'
            ],
            [
                'county_id' => 25,
                'sub_county_name' => 'Samburu North'
            ],
            [
                'county_id' => 26,
                'sub_county_name' => 'Trans Nzoia West'
            ],
            [
                'county_id' => 26,
                'sub_county_name' => 'Trans Nzoia East'
            ],
            [
                'county_id' => 26,
                'sub_county_name' => 'Kwanza'
            ],
            [
                'county_id' => 26,
                'sub_county_name' => 'Endebess'
            ],
            [
                'county_id' => 26,
                'sub_county_name' => 'Kiminini'
            ],
            [
                'county_id' => 27,
                'sub_county_name' => 'Ainabkoi'
            ],
            [
                'county_id' => 27,
                'sub_county_name' => 'Kapseret'
            ],
            [
                'county_id' => 27,
                'sub_county_name' => 'Kesses'
            ],
            [
                'county_id' => 27,
                'sub_county_name' => 'Moiben'
            ],
            [
                'county_id' => 27,
                'sub_county_name' => 'Soy'
            ],
            [
                'county_id' => 27,
                'sub_county_name' => 'Turbo'
            ],
            [
                'county_id' => 28,
                'sub_county_name' => 'Keiyo NOrth'
            ],
            [
                'county_id' => 28,
                'sub_county_name' => 'Keiyo South'
            ],
            [
                'county_id' => 28,
                'sub_county_name' => 'Marakwet East'
            ],
            [
                'county_id' => 28,
                'sub_county_name' => 'Marakwet West'
            ],
            [
                'county_id' => 29,
                'sub_county_name' => 'Chesumei'
            ],
            [
                'county_id' => 29,
                'sub_county_name' => 'Nandi Central'
            ],
            [
                'county_id' => 29,
                'sub_county_name' => 'Nandi East'
            ],
            [
                'county_id' => 29,
                'sub_county_name' => 'Nandi North'
            ],
            [
                'county_id' => 29,
                'sub_county_name' => 'Nandi South'
            ],
            [
                'county_id' => 29,
                'sub_county_name' => 'Tinderet'
            ],
            [
                'county_id' => 30,
                'sub_county_name' => 'Baringo Central'
            ],
            [
                'county_id' => 30,
                'sub_county_name' => 'Baringo North'
            ],
            [
                'county_id' => 30,
                'sub_county_name' => 'East Pokot'
            ],
            [
                'county_id' => 30,
                'sub_county_name' => 'Koibatek'
            ],
            [
                'county_id' => 30,
                'sub_county_name' => 'Marigat'
            ],
            [
                'county_id' => 30,
                'sub_county_name' => 'Mogotio'
            ],
            [
                'county_id' => 30,
                'sub_county_name' => 'Tiaty East'
            ],
            [
                'county_id' => 31,
                'sub_county_name' => 'Laikipia Central'
            ],
            [
                'county_id' => 31,
                'sub_county_name' => 'Laikipia East'
            ],
            [
                'county_id' => 31,
                'sub_county_name' => 'Laikipia North'
            ],
            [
                'county_id' => 31,
                'sub_county_name' => 'Laikipia West'
            ],
            [
                'county_id' => 31,
                'sub_county_name' => 'Nyahururu'
            ],
            [
                'county_id' => 32,
                'sub_county_name' => 'Gilgil'
            ],
            [
                'county_id' => 32,
                'sub_county_name' => 'Kuresoi North'
            ],
            [
                'county_id' => 32,
                'sub_county_name' => 'Kuseroi South'
            ],
            [
                'county_id' => 32,
                'sub_county_name' => 'Molo'
            ],
            [
                'county_id' => 32,
                'sub_county_name' => 'Naivasha'
            ],
            [
                'county_id' => 32,
                'sub_county_name' => 'Nakuru East'
            ],
            [
                'county_id' => 32,
                'sub_county_name' => 'Nakuru North'
            ],
            [
                'county_id' => 32,
                'sub_county_name' => 'Nakuru West'
            ],
            [
                'county_id' => 32,
                'sub_county_name' => 'Njoro'
            ],
            [
                'county_id' => 32,
                'sub_county_name' => 'Rongai'
            ],
            [
                'county_id' => 32,
                'sub_county_name' => 'Subukia'
            ],
            [
                'county_id' => 33,
                'sub_county_name' => 'Narok East'
            ],
            [
                'county_id' => 33,
                'sub_county_name' => 'Narok North'
            ],
            [
                'county_id' => 33,
                'sub_county_name' => 'Narok South'
            ],
            [
                'county_id' => 33,
                'sub_county_name' => 'Narok West'
            ],
            [
                'county_id' => 33,
                'sub_county_name' => 'Trans Mara East'
            ],
            [
                'county_id' => 33,
                'sub_county_name' => 'Trans Mara West'
            ],
            [
                'county_id' => 33,
                'sub_county_name' => 'Mau Forest'
            ],
            [
                'county_id' => 34,
                'sub_county_name' => 'Kajiado Central'
            ],
            [
                'county_id' => 34,
                'sub_county_name' => 'Kajiado North'
            ],
            [
                'county_id' => 34,
                'sub_county_name' => 'Kajiado West'
            ],
            [
                'county_id' => 34,
                'sub_county_name' => 'Loitokitok'
            ],
            [
                'county_id' => 34,
                'sub_county_name' => 'Mashuuru'
            ],
            [
                'county_id' => 34,
                'sub_county_name' => 'Isinya'
            ],
            [
                'county_id' => 35,
                'sub_county_name' => 'Belgut'
            ],
            [
                'county_id' => 35,
                'sub_county_name' => 'Bureti'
            ],
            [
                'county_id' => 35,
                'sub_county_name' => 'Kericho East'
            ],
            [
                'county_id' => 35,
                'sub_county_name' => 'Kipkelion'
            ],
            [
                'county_id' => 35,
                'sub_county_name' => 'Londiani'
            ],
            [
                'county_id' => 35,
                'sub_county_name' => 'Soin Sigowet'
            ],
            [
                'county_id' => 36,
                'sub_county_name' => 'Bomet East'
            ],
            [
                'county_id' => 36,
                'sub_county_name' => 'Chepalungu'
            ],
            [
                'county_id' => 36,
                'sub_county_name' => 'Konoin'
            ],
            [
                'county_id' => 36,
                'sub_county_name' => 'Sotik'
            ],
            [
                'county_id' => 36,
                'sub_county_name' => 'Bomet Central'
            ],
            [
                'county_id' => 37,
                'sub_county_name' => 'Butere'
            ],
            [
                'county_id' => 37,
                'sub_county_name' => 'Kakamega Central'
            ],
            [
                'county_id' => 37,
                'sub_county_name' => 'Kakamega East'
            ],
            [
                'county_id' => 37,
                'sub_county_name' => 'Kakamega North'
            ],
            [
                'county_id' => 37,
                'sub_county_name' => 'Kakamega South'
            ],
            [
                'county_id' => 37,
                'sub_county_name' => 'Khwisero'
            ],
            [
                'county_id' => 37,
                'sub_county_name' => 'Likuyani'
            ],
            [
                'county_id' => 37,
                'sub_county_name' => 'Lugari'
            ],
            [
                'county_id' => 37,
                'sub_county_name' => 'Matete'
            ],
            [
                'county_id' => 37,
                'sub_county_name' => 'Matungu'
            ],
            [
                'county_id' => 37,
                'sub_county_name' => 'Mumias East'
            ],
            [
                'county_id' => 37,
                'sub_county_name' => 'Mumias West'
            ],
            [
                'county_id' => 37,
                'sub_county_name' => 'Navakholo'
            ],
            [
                'county_id' => 38,
                'sub_county_name' => 'Emuhaya'
            ],
            [
                'county_id' => 38,
                'sub_county_name' => 'Vihiga'
            ],
            [
                'county_id' => 38,
                'sub_county_name' => 'Sabatia'
            ],
            [
                'county_id' => 38,
                'sub_county_name' => 'Luanda'
            ],
            [
                'county_id' => 38,
                'sub_county_name' => 'Hamisi'
            ],
            [
                'county_id' => 38,
                'sub_county_name' => 'Kakamega Forest'
            ],
            [
                'county_id' => 39,
                'sub_county_name' => 'Bumula'
            ],
            [
                'county_id' => 39,
                'sub_county_name' => 'Bungoma Central'
            ],
            [
                'county_id' => 39,
                'sub_county_name' => 'Bungoma East'
            ],
            [
                'county_id' => 39,
                'sub_county_name' => 'Bungoma North'
            ],
            [
                'county_id' => 39,
                'sub_county_name' => 'Bungoma South'
            ],
            [
                'county_id' => 39,
                'sub_county_name' => 'Cheptais'
            ],
            [
                'county_id' => 39,
                'sub_county_name' => 'Kimilili Bungoma'
            ],
            [
                'county_id' => 39,
                'sub_county_name' => 'Mt. Elgon'
            ],
            [
                'county_id' => 39,
                'sub_county_name' => 'Bungoma West'
            ],
            [
                'county_id' => 39,
                'sub_county_name' => 'Tongaren'
            ],
            [
                'county_id' => 39,
                'sub_county_name' => 'Webuye West'
            ],
            [
                'county_id' => 39,
                'sub_county_name' => 'Mt. Elgon Forest'
            ],
            [
                'county_id' => 40,
                'sub_county_name' => 'Bunyala'
            ],
            [
                'county_id' => 40,
                'sub_county_name' => 'Busia'
            ],
            [
                'county_id' => 40,
                'sub_county_name' => 'Butula'
            ],
            [
                'county_id' => 40,
                'sub_county_name' => 'Nambale'
            ],
            [
                'county_id' => 40,
                'sub_county_name' => 'Samia'
            ],
            [
                'county_id' => 40,
                'sub_county_name' => 'Teso North'
            ],
            [
                'county_id' => 40,
                'sub_county_name' => 'Teso South'
            ],
            [
                'county_id' => 41,
                'sub_county_name' => 'Siaya'
            ],
            [
                'county_id' => 41,
                'sub_county_name' => 'Gem'
            ],
            [
                'county_id' => 41,
                'sub_county_name' => 'Ugenya'
            ],
            [
                'county_id' => 41,
                'sub_county_name' => 'Ugunja'
            ],
            [
                'county_id' => 41,
                'sub_county_name' => 'Bondo'
            ],
            [
                'county_id' => 41,
                'sub_county_name' => 'Rarieda'
            ],
            [
                'county_id' => 42,
                'sub_county_name' => 'Kisumu East'
            ],
            [
                'county_id' => 42,
                'sub_county_name' => 'Kisumu Central'
            ],
            [
                'county_id' => 42,
                'sub_county_name' => 'Kisumu West'
            ],
            [
                'county_id' => 42,
                'sub_county_name' => 'Seme'
            ],
            [
                'county_id' => 42,
                'sub_county_name' => 'Muhuroni'
            ],
            [
                'county_id' => 42,
                'sub_county_name' => 'Nyando'
            ],
            [
                'county_id' => 42,
                'sub_county_name' => 'Nyakach'
            ],
            [
                'county_id' => 43,
                'sub_county_name' => 'Homa Bay'
            ],
            [
                'county_id' => 43,
                'sub_county_name' => 'Ndhiwa'
            ],
            [
                'county_id' => 43,
                'sub_county_name' => 'Rachuonyo North'
            ],
            [
                'county_id' => 43,
                'sub_county_name' => 'Rachonyo East'
            ],
            [
                'county_id' => 43,
                'sub_county_name' => 'Rachuonyo South'
            ],
            [
                'county_id' => 43,
                'sub_county_name' => 'Rangwe'
            ],
            [
                'county_id' => 43,
                'sub_county_name' => 'Suba North'
            ],
            [
                'county_id' => 43,
                'sub_county_name' => 'Suba South'
            ],
            [
                'county_id' => 44,
                'sub_county_name' => 'Awendo'
            ],
            [
                'county_id' => 44,
                'sub_county_name' => 'Kuria East'
            ],
            [
                'county_id' => 44,
                'sub_county_name' => 'Kuria West'
            ],
            [
                'county_id' => 44,
                'sub_county_name' => 'Nyatike'
            ],
            [
                'county_id' => 44,
                'sub_county_name' => 'Rongo'
            ],
            [
                'county_id' => 44,
                'sub_county_name' => 'Suna East'
            ],
            [
                'county_id' => 44,
                'sub_county_name' => 'Suna West'
            ],
            [
                'county_id' => 44,
                'sub_county_name' => 'Uriri'
            ],
            [
                'county_id' => 45,
                'sub_county_name' => 'Etago'
            ],
            [
                'county_id' => 45,
                'sub_county_name' => 'Gucha'
            ],
            [
                'county_id' => 45,
                'sub_county_name' => 'Gucha South'
            ],
            [
                'county_id' => 45,
                'sub_county_name' => 'Kenyenya'
            ],
            [
                'county_id' => 45,
                'sub_county_name' => 'Kicii Central'
            ],
            [
                'county_id' => 45,
                'sub_county_name' => 'Kisii South'
            ],
            [
                'county_id' => 45,
                'sub_county_name' => 'Kitutu Central'
            ],
            [
                'county_id' => 45,
                'sub_county_name' => 'Marani'
            ],
            [
                'county_id' => 45,
                'sub_county_name' => 'Masaba South'
            ],
            [
                'county_id' => 45,
                'sub_county_name' => 'Nyamache'
            ],
            [
                'county_id' => 45,
                'sub_county_name' => 'Sameta'
            ],
            [
                'county_id' => 46,
                'sub_county_name' => 'Borabu'
            ],
            [
                'county_id' => 46,
                'sub_county_name' => 'Manga'
            ],
            [
                'county_id' => 46,
                'sub_county_name' => 'Masaba North'
            ],
            [
                'county_id' => 46,
                'sub_county_name' => 'Nyamira North'
            ],
            [
                'county_id' => 46,
                'sub_county_name' => 'Nyamira South'
            ],
            [
                'county_id' => 47,
                'sub_county_name' => 'Dagoretti'
            ],
            [
                'county_id' => 47,
                'sub_county_name' => 'Embakasi'
            ],
            [
                'county_id' => 47,
                'sub_county_name' => 'Kamukunji'
            ],
            [
                'county_id' => 47,
                'sub_county_name' => 'Kasarani'
            ],
            [
                'county_id' => 47,
                'sub_county_name' => 'Kibra'
            ],
            [
                'county_id' => 47,
                'sub_county_name' => "Lang'ata"
            ],
            [
                'county_id' => 47,
                'sub_county_name' => 'Madaraka'
            ],
            [
                'county_id' => 47,
                'sub_county_name' => 'Mathare'
            ],
            [
                'county_id' => 47,
                'sub_county_name' => 'Njiru'
            ],
            [
                'county_id' => 47,
                'sub_county_name' => 'Starehe'
            ],
            [
                'county_id' => 47,
                'sub_county_name' => 'Westlands'
            ]
        ];

        foreach ($sub_counties as $value) {
            SubCounties::create([
                'county_id' => $value['county_id'],
                'sub_county_name' => $value['sub_county_name']
            ]);
        }
    }
}
