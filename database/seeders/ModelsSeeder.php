<?php

namespace Database\Seeders;

use App\Models\Subsidiary;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class ModelsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $subcategories = [
            [
                'ad_make_id' => 1,
                'model' => 'Avalon'
            ],  //Toyota
            [
                'ad_make_id' => 1,
                'model' => 'Camry'
            ],  //Toyota
            [
                'ad_make_id' => 1,
                'model' => 'Corolla'
            ],  //Toyota
            [
                'ad_make_id' => 1,
                'model' => 'Prius'
            ],  //Toyota
            [
                'ad_make_id' => 1,
                'model' => 'Yaris'
            ],  //Toyota
            [
                'ad_make_id' => 1,
                'model' => '86'
            ],  //Toyota
            [
                'ad_make_id' => 1,
                'model' => 'Sienna'
            ],  //Toyota
            [
                'ad_make_id' => 1,
                'model' => 'C-HR'
            ],  //Toyota
            [
                'ad_make_id' => 1,
                'model' => 'RAV 4'
            ],  //Toyota
            [
                'ad_make_id' => 1,
                'model' => 'Highlander'
            ],  //Toyota
            [
                'ad_make_id' => 1,
                'model' => '4Runner'
            ],  //Toyota
            [
                'ad_make_id' => 1,
                'model' => 'Tacoma'
            ],  //Toyota
            [
                'ad_make_id' => 1,
                'model' => 'Tundra'
            ],  //Toyota
            [
                'ad_make_id' => 2,
                'model' => 'Murano'
            ], //Nissan
            [
                'ad_make_id' => 3,
                'model' => 'Model...'
            ],  //Isuzu
            [
                'ad_make_id' => 4,
                'model' => 'Model...'
            ],  //Mazda
            [
                'ad_make_id' => 5,
                'model' => 'Model...'
            ],  //Suzuki
            [
                'ad_make_id' => 6,
                'model' => 'Model...'
            ],  //Chevrolet
        ];

        foreach ($subcategories as $category) {
            Subsidiary::create([
                'ad_make_id' => $category['ad_make_id'],
                'model' => $category['model'],
                'model_ref' => Str::slug($category['model']),
            ]);
        }
    }
}
