<?php

namespace Database\Seeders;

use App\Models\AdMake;
use App\Models\Subsidiary;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            CountiesSeeder::class,
            SubCountiesSeeder::class,
            MakeSeeder::class,
            ModelsSeeder::class,
        ]);
    }
}
