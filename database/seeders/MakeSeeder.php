<?php

namespace Database\Seeders;

use App\Models\AdMake;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class MakeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $makes = [
            ['make' => 'Toyota'],
            ['make' => 'Nissan'],
            ['make' => 'Isuzu'],
            ['make' => 'Mazda'],
            ['make' => 'Suzuki'],
            ['make' => 'Chevrolet'],
        ];

        foreach ($makes as $make) {
            AdMake::create([
                'make' => $make['make'],
                'make_ref' => Str::slug($make['make']),
            ]);
        }
    }
}
