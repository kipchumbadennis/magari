<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\MagariController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Image;


Route::get('/',[MagariController::class, 'index'])->name('homepage');

Route::get('{make}/{model}/{slug}', [MagariController::class, 'showAd'])->name('ad.show');
Route::get('{slug}', [MagariController::class, 'showMakes'])->name('make.show');

Route::post('searches', [MagariController::class, 'showSearches'])->name('searches.show');
Route::post('Search', [MagariController::class, 'searchAds'])->name('search');
Route::get('Post/Ad', [MagariController::class, 'create'])->name('ad.form');
Route::post('Post/Ad', [MagariController::class, 'store'])->name('post.store');
Route::post('Response', [MagariController::class, 'cloudinaryResponse'])->name('cloud.response');
Route::get('show/{id}', [MagariController::class, 'show']);
Route::get('subcounty/{id}', [MagariController::class, 'subCounties']);
Route::get('image', function () {
    $img = Image::make('foo.jpg')->resize(300, 200);
    return $img->response('jpg');
});

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');
